data = document.querySelector('pre').textContent.trim().split("\n\n")
draw = data[0].trim().split(',').filter(Boolean).map(s => Number(s))
boardsRaw = data.slice(1)
boards = boardsRaw.map(b => { 
	return b.split('\n')
			.map(x => x.split(' ')
						.filter(Boolean)
						.map(n => {
							let num = Number(n);
							let hit = false;
							if (num == draw[0] || num == draw[1] || num == draw[2] || num == draw[3] || num == draw[4])
								hit = true;
							return { hit, num }
						}
					)
				)
		});

let winning_board = -1;

function draw_number(num)
{
	console.log("Draw number: " + num);
	boards.forEach(board => {
		board.forEach(row => {
			for (var i = 4; i >= 0; i--) {
				if (row[i].num == num)
				{
					row[i].hit = true;
				}
			}
		})
	})
}

let boards_that_won = [];

function check_win(part)
{
	let num_wins = 0;
	for (let boardIndex = boards.length - 1; boardIndex >= 0; boardIndex--) {
		let board = boards[boardIndex];

		let colhit = false;
		let rowhit = false;
		for (let rowIndex = board.length - 1; rowIndex >= 0; rowIndex--) {
			if (board[rowIndex].filter(col => col.hit == true).length == 5)
			{
				rowhit = true;
			}
		}
		for (let i = 4; i >= 0; i--)
		{
			if (board[0][i].hit && 
				board[1][i].hit && 
				board[2][i].hit && 
				board[3][i].hit && 
				board[4][i].hit)
			{
				colhit = true;
			}
		}
		if (rowhit || colhit)
		{
			num_wins++;
			winning_board = boardIndex;
			if (boards_that_won.indexOf(boardIndex) == -1)
				boards_that_won.push(boardIndex);
			if (part == 1)
				console.log("Board " + boardIndex + " wins!");
		}
	}
	if (part == 1)
	{
		if (num_wins > 0)
			console.log("We have a winner!");
		else
			console.log("No winner yet!!");
	}
	return (winning_board);
}

function run_draw_simulation_pt1()
{
	for (var i = 0; i < draw.length; i++) {
		draw_number(draw[i]);
		if(check_win(1) != -1)
		{
			let last_num = draw[i]; // reminder..
			let sum_not_hit = get_total_from_board_and_num(winning_board);
			console.log("Result part1: " + sum_not_hit * last_num);
			break ;
		}
	}
}

function get_total_from_board_and_num(board)
{
	let winner = boards[board];
	let sum_not_hit = 0;
	for (var rowIndex = winner.length - 1; rowIndex >= 0; rowIndex--) {
		winner[rowIndex].filter(el => !el.hit).forEach(el => sum_not_hit += el.num);
	}
	return sum_not_hit;
}

function get_last_winner()
{
	let last_num_drawn = 0;
	for (var i = 0; i < draw.length; i++) {
		draw_number(draw[i]);
		check_win(2);
		if (boards_that_won.length == 100)
		{
			last_num_drawn = draw[i];
			break ;
		}
	}
	console.log("Result part2: " +
		last_num_drawn *
		get_total_from_board_and_num(boards_that_won[boards_that_won.length - 1])
	);
}
