use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("input")?;
    let reader = BufReader::new(f);
    let mut nums: Vec<i32> = Vec::new();

    for line in reader.lines() {
        let buffer = line.unwrap();
        nums.push(buffer.parse::<i32>().unwrap());
    }

    let windows = nums.windows(2);
    let mut inc = 0;
    // let mut dec = 0;
    for win in windows {
        if win[1] > win[0]
        {
            inc = inc + 1;
        }
    }
    println!("Part1: {:?}", inc);

    let windows_part2: Vec<i32> = nums.windows(3).map(|x| x[0] + x[1] + x[2]).collect();
    let windows_part2a = windows_part2.windows(2);
    let mut inc_p2f = 0;
    for win in windows_part2a {
        if win[1] > win[0]
        {
            inc_p2f = inc_p2f + 1;
        }
    }
    println!("Part2: {:?}", inc_p2f);

    Ok(())
}
