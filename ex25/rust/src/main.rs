use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn print_seacucumbers(seacucumbers: &Vec<Vec<(char,bool)>>)
{
    for cucumber in seacucumbers {
        println!("{:?}", String::from_utf8(cucumber.iter().map(|t| t.0 as u8).collect::<Vec<u8>>()).unwrap());
    }
}

fn move_east(mut seacucumbers: Vec<Vec<(char,bool)>>, width: usize, height: usize, num_moves: &mut usize) -> Vec<Vec<(char, bool)>> {
    for h in 0..height {
        for index in 0..width - 1 {
            if seacucumbers[h][index].0 == '>' && seacucumbers[h][index + 1].0 == '.'
            {
                seacucumbers[h][index].1 = true;
            }
        }
        if seacucumbers[h][width - 1].0 == '>' && seacucumbers[h][0].0 == '.'
        {
            seacucumbers[h][width - 1].0 = '.';
            seacucumbers[h][0].0 = '>';
            *num_moves += 1;
        }
        for index in 0..width - 1 {
            if seacucumbers[h][index].1 == true
            {
                seacucumbers[h][index].1 = false;
                seacucumbers[h][index].0 = '.';
                seacucumbers[h][index + 1].0 = '>';
                *num_moves += 1;
            }
        }
    }
    return seacucumbers;
}

fn move_south(mut seacucumbers: Vec<Vec<(char,bool)>>, width: usize, height: usize, num_moves: &mut usize) -> Vec<Vec<(char, bool)>> {
    for w in 0..width {
        for index in 0..height - 1 {
            if seacucumbers[index][w].0 == 'v' && seacucumbers[index + 1][w].0 == '.'
            {
                seacucumbers[index][w].1 = true;
            }
        }
        if seacucumbers[height - 1][w].0 == 'v' && seacucumbers[0][w].0 == '.'
        {
            seacucumbers[height - 1][w].0 = '.';
            seacucumbers[0][w].0 = 'v';
            *num_moves += 1;
        }
        for index in 0..height - 1 {
            if seacucumbers[index][w].1 == true
            {
                seacucumbers[index][w].1 = false;
                seacucumbers[index][w].0 = '.';
                seacucumbers[index + 1][w].0 = 'v';
                *num_moves += 1;
            }
        }
    }
    return seacucumbers;
}

fn main() -> io::Result<()> {
    let print_all = false;
    let f = File::open("../input")?;
    // let f = File::open("../simple")?;
    let reader = BufReader::new(f);
    let mut seacucumbers: Vec<Vec<(char,bool)>> = Vec::new();

    for line in reader.lines() {
        seacucumbers.push(line.unwrap().chars().map(|c| (c,false)).collect::<Vec<(char,bool)>>());
    }

    let width = seacucumbers[0].len();
    let height = seacucumbers.len();

    let mut num_moves_east: usize = 1;
    let mut num_moves_south: usize = 1;
    let mut total_moves: usize = 0;

    let mut steps: usize = 0;

    while !(num_moves_east == 0 && num_moves_south == 0) && steps < 1000 {
        num_moves_east = 0;
        num_moves_south = 0;
        seacucumbers = move_east(seacucumbers, width, height, &mut num_moves_east);
        seacucumbers = move_south(seacucumbers, width, height, &mut num_moves_south);
        if print_all
        {
            println!("\nstep {:?} ({:?},{:?})", steps, num_moves_south, num_moves_east);
            print_seacucumbers(&seacucumbers);
        }
        total_moves += num_moves_south + num_moves_east;
        steps += 1;
    }

    if print_all
    {
        println!("\nend state");
        print_seacucumbers(&seacucumbers);
    }
        print_seacucumbers(&seacucumbers);

    println!("Total steps: {:?} ({:?},{:?})", steps, num_moves_south, num_moves_east);
    println!("Total moves: {:?}", total_moves);

    Ok(())
}
