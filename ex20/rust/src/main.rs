use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn is_bit_on(c: char) -> usize {
	if c == '#'
	{
		return 1;
	}
	return 0;
}

//30091 == too high

fn main() -> io::Result<()> {
	let f = File::open("../input")?;
	// let f = File::open("../simple")?;
	let reader = BufReader::new(f);

	let mut enhancement_algo: String = String::with_capacity(513);

	let mut image: Vec<String> = Vec::new();

	for (n, line) in reader.lines().enumerate()
	{
		if n == 0
		{
			let algo = line.as_ref();
			enhancement_algo = algo.unwrap().to_string();
		}
		if n > 1
		{
			let mut line_to_grow = line.unwrap();
			line_to_grow.insert_str(0, "........................................................................................................................................................................................................");	// adds 10 empty spots at the start
			line_to_grow.push_str("........................................................................................................................................................................................................");		// adds 10 empty spots in the end
			image.push(line_to_grow);
		}
	}

	// let offset_x = 1;
	// let offset_y = 1;

	// also add a 10 empty lines before and after
	let mut filler_lines = "........................................................................................................................................................................................................".to_string();	// the 20 emtpy spots already in a line
	for _n in 0..(image[0].len() - 200) {						// the rest of the empty spots are in a loop to match line length
		filler_lines.push('.');
	}
	for _n in 0..210 {
		image.push(filler_lines.clone());
	}
	image.rotate_right(100);

	let image_width = image[0].len();
	let image_height = image.len();

	let mut image_to_change = image.clone();
	let iterations = 50;

	for _i in 0..iterations {
		let image_source = image_to_change.clone();
		image_to_change = image_source.clone();
		println!("iteration: {:?}", _i);
		for y in 1..(image_height - 1) {
			for x in 1..(image_width - 1) {
				let mut index: usize = 0;
				index |= is_bit_on(image_source[y - 1].chars().nth(x - 1).unwrap()) << 8;
				index |= is_bit_on(image_source[y - 1].chars().nth(x).unwrap()) << 7;
				index |= is_bit_on(image_source[y - 1].chars().nth(x + 1).unwrap()) << 6;
				index |= is_bit_on(image_source[y].chars().nth(x - 1).unwrap()) << 5;
				index |= is_bit_on(image_source[y].chars().nth(x).unwrap()) << 4;
				index |= is_bit_on(image_source[y].chars().nth(x + 1).unwrap()) << 3;
				index |= is_bit_on(image_source[y + 1].chars().nth(x - 1).unwrap()) << 2;
				index |= is_bit_on(image_source[y + 1].chars().nth(x).unwrap()) << 1;
				index |= is_bit_on(image_source[y + 1].chars().nth(x + 1).unwrap());
				image_to_change[y].remove(x);
				image_to_change[y].insert(x, enhancement_algo.chars().nth(index).unwrap());
			}
		}
		// offset_x -= 1;
		// offset_y -= 1;
		// image_height += 2;
		// image_width += 2;
	}

	// fix border
	for y in 1..(image_height - 1)
	{
		image_to_change[y].remove(1);
		image_to_change[y].insert(1, '.');
		image_to_change[y].remove(image_width - 2);
		image_to_change[y].push('.');
	}
	for x in 1..(image_width - 1)
	{
		image_to_change[image_height - 2].remove(x);
		image_to_change[image_height - 2].insert(x, '.');
	}

	let lit_pixels = image_to_change.iter().fold(0, |acc, item| acc + item.chars().filter(|c| *c == '#').count());


	// println!("{:?}", enhancement_algo);
	// println!("{:#?}", image);
	println!("{:#?}", image_to_change);
	println!("lit pixels: {:?}", lit_pixels);

	Ok(())
}
