use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("input")?;
    let reader = BufReader::new(f);
    let mut data: Vec<String> = Vec::new();

    let mut grid = [0 as u32; 12];
    let mut num_lines = 0;

    for line in reader.lines() {
        data.push(line.as_ref().unwrap().to_string());
        let mut str_as_chars = line.as_ref().unwrap().chars();
        let len = line.as_ref().unwrap().chars().count();
        for n in 0..len {
            if str_as_chars.next().unwrap() == '1'
            {
                grid[n] = grid[n] + 1;
            }
        }
        num_lines = num_lines + 1;
    }

    let shift = [11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0];
    let mut gamma: i32 = 0;
    let mut epsilon: i32 = 0;

    // let oxygen_clone: Vec<String> = data.clone();
    // let mut oxygen: Vec<&String>;
    // let mut co2: Vec<String> = data.to_vec();

    for c in 0..12 {
        // oxygen = oxygen_clone.iter().filter(|x| x.chars().nth(c).unwrap() == '1').collect::<Vec<&String>>();
        if grid[c] >= num_lines / 2
        {
            gamma = gamma + (1 << shift[c]);
                            // iter().filter(|s| (s.chars().nth(0).unwrap() == '1')).count());
        }
        else
        {
            epsilon = epsilon + (1 << shift[c]);
        }
    }

    let mut strmatch_1: String = "".to_string();
    let mut len_1;
    for _n in 0..12 {
        // println!("{:?}", strmatch_1);
        strmatch_1.push('1');
        let count_1 = data.iter().filter(|x| x.starts_with(strmatch_1.as_str())).count();
        // println!("1:{:?}", data.iter().filter(|x| x.starts_with(strmatch_1.as_str())).collect::<Vec<&String>>());
        strmatch_1.pop();
        strmatch_1.push('0');
        let count_0 = data.iter().filter(|x| x.starts_with(strmatch_1.as_str())).count();
        // println!("0:{:?}", data.iter().filter(|x| x.starts_with(strmatch_1.as_str())).collect::<Vec<&String>>());

        if count_1 >= count_0
        {
            strmatch_1.pop();
            strmatch_1.push('1');
        }

        len_1 = data.iter().filter(|x| x.starts_with(strmatch_1.as_str())).count();
        if len_1 == 1
        {
            println!("len_1==1({:?}): {:?}", len_1, strmatch_1);
            break ;
        }
    }

    let mut strmatch_0: String = "".to_string();
    let mut len_0;
    for _n in 0..12 {
        println!("{:?}", strmatch_0);
        strmatch_0.push('1');
        let count_1 = data.iter().filter(|x| x.starts_with(strmatch_0.as_str())).count();
        println!("1:{:?}", data.iter().filter(|x| x.starts_with(strmatch_0.as_str())).collect::<Vec<&String>>());
        strmatch_0.pop();
        strmatch_0.push('0');
        let count_0 = data.iter().filter(|x| x.starts_with(strmatch_0.as_str())).count();
        println!("0:{:?}", data.iter().filter(|x| x.starts_with(strmatch_0.as_str())).collect::<Vec<&String>>());

        if count_1 < count_0
        {
            strmatch_0.pop();
            strmatch_0.push('1');
        }

        len_0 = data.iter().filter(|x| x.starts_with(strmatch_0.as_str())).count();
        if len_0 == 1
        {
            println!("len_0==1({:?}): {:?}", len_0, strmatch_0);
            break ;
        }
    }

    println!("strmatch1: {:?} = 509", strmatch_1);
    println!("strmatch2: {:?} = 336", strmatch_0);

// 000111111101 = 254
// 101010011011 = 2715
// 254 * 2715 = 689610

// not 172551 (509*339) is too low



    // for n in 0..12
    // {
    //     let count_1 = data.iter().filter(|x| x.chars().nth(n).unwrap() == '1').count();
    //     let count_0 = data.iter().filter(|x| x.chars().nth(n).unwrap() == '0').count();
    //     if count_1 >= count_0
    //     {

    //     } else {
    //         unimplemented!();
    //     }
    // }

    println!("Part 1");
    println!("Gamma: {:?}", gamma);
    println!("Epsilon: {:?}", epsilon);
    println!("Multiplied: {:?}", gamma * epsilon);

    // println!("{:?}", data.iter().filter(|s| s.chars().nth(0).unwrap() == '1').collect::<Vec<&String>>());
    // println!("{:?}", data.first().unwrap().get(0..1).unwrap() == "1");
    // println!("{:?}", step0_1);
    // println!("{:?}", step0_0);


    Ok(())
}