use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("input")?;
    let reader = BufReader::new(f);
    let mut data: Vec<String> = Vec::new();

    let mut aim: i32 = 0;
    let mut forward_amount: i32 = 0;
    let mut depth_amount: i32 = 0;

    for line in reader.lines() {
        let buffer = line.as_ref().unwrap();
        data.push(buffer.to_string());

        let num = buffer.as_str().split(' ').last().unwrap().parse::<i32>().unwrap();

        if buffer.as_str().starts_with("forward")
        {
            forward_amount = forward_amount + num;
            if aim != 0
            {
                depth_amount = depth_amount + (aim * num);
            }
        }
        if buffer.as_str().starts_with("up")
        {
            aim = aim - num;
        }
        if buffer.as_str().starts_with("down")
        {
            aim = aim + num;
        }

    }

    let forward = data.iter()
                    .filter(|x| x.starts_with("forward"))
                    .map(|x| x.split(' ').last().unwrap().parse::<i32>().unwrap())
                    .reduce(|a,b| a + b);
    let down = data.iter()
                .filter(|x| x.starts_with("down"))
                .map(|x| x.split(' ').last().unwrap().parse::<i32>().unwrap())
                .reduce(|a,b| a + b);
    let up = data.iter()
                .filter(|x| x.starts_with("up"))
                .map(|x| x.split(' ').last().unwrap().parse::<i32>().unwrap())
                .reduce(|a,b| a + b);

    println!("Part1\nhorizontal: {:?} depth: {:?}", down.unwrap() - up.unwrap(), forward.unwrap());
    println!("result: {:?}", (down.unwrap() - up.unwrap()) * forward.unwrap());


    println!("\nPart2\nforward: {:?} depth: {:?}", forward_amount, depth_amount);
    println!("result: {:?}", forward_amount * depth_amount);

    Ok(())
}
