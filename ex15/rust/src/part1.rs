use petgraph::Graph;
use petgraph::algo::astar;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("../input")?;
    // let f = File::open("../simple")?;
    let reader = BufReader::new(f);
    let mut lines: Vec<Vec<i32>> = Vec::new();


    // let mut y = 0.;
    for line in reader.lines() {
        let mut row: Vec<i32> = Vec::new();
        let line_len = line.as_ref().unwrap().len();
        let chars = line.unwrap();
        for i in 0..line_len {
            row.push(chars.chars().nth(i).unwrap().to_string().parse().unwrap());
        }
        lines.push(row);
    }

    let mut graph = Graph::new();
    let mut nodes: Vec<Vec<_>> = Vec::new();
    for (y, line) in lines.iter().enumerate()
    {
        nodes.push(Vec::new());
        for (x, _c) in line.iter().enumerate()
        {
            nodes[y].push(graph.add_node((x as i32, y as i32), _c));
        }
    }

    for (y, line) in lines.iter().enumerate()
    {
        for (x, weight) in line.iter().enumerate()
        {
            if y > 0
            {
                graph.add_edge(nodes[y - 1][x], nodes[y][x], 0);
            }
            if x > 0
            {
                graph.add_edge(nodes[y][x - 1], nodes[y][x], 0);
            }
        }
    }

    let width = lines[0].len();
    let height = lines.len();

    let path = astar(&graph, nodes[0][0], |finish| finish == nodes[height - 1][width - 1], |e| *e.weight(), |_| 0);
    println!("{:#?}", path);


    Ok(())
}