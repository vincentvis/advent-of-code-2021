use petgraph::Graph;
use petgraph::algo::astar;
// use petgraph::algo::dijkstra;
// use petgraph::algo::bellman_ford;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    // let f = File::open("../asja")?;
    // let f = File::open("../input")?;
    let f = File::open("../simple")?;
    let reader = BufReader::new(f);
    let mut lines: Vec<Vec<i32>> = Vec::new();

    let repeat = false;


    // let mut y = 0.;
    for line in reader.lines() {
        let mut row: Vec<i32> = Vec::new();
        let line_len = line.as_ref().unwrap().len();
        let chars = line.unwrap();
        for i in 0..line_len {
            row.push(chars.chars().nth(i).unwrap().to_string().parse().unwrap());
        }
        if repeat {
            for add in 1..5 {
                for i in 0..line_len {
                    let mut num: i32 = chars.chars().nth(i).unwrap().to_string().parse().unwrap();
                    num += add as i32;
                    if num > 9
                    {
                        num -= 9;
                    }
                    row.push(num);
                }
            }
        }
        lines.push(row);
    }

    if repeat
    {
        let tempheight = lines.len();
        for add_rows in 1..5 {
            for row in 0..tempheight {
                let mut newrow: Vec<i32> = Vec::new();
                for x in lines[row].iter()
                {
                    let mut newnum = *x;
                    newnum += add_rows as i32;
                    if newnum > 9
                    {
                        newnum -= 9;
                    }
                    newrow.push(newnum);
                }
                lines.push(newrow);
            }
        }
    }

    let width = lines[0].len();
    let height = lines.len();

    // let mut graph = Graph::new();
    let mut graph = Graph::<_,_, petgraph::Undirected>::new_undirected();
    let mut nodes: Vec<Vec<_>> = Vec::new();
    for (y, line) in lines.iter().enumerate()
    {
        nodes.push(Vec::new());
        for (x, weight) in line.iter().enumerate()
        {
            let newnode = graph.add_node(weight);
            let weight = graph.node_weight(newnode);
            println!("[{:?},{:?}] = {:?}", x, y, weight);
            nodes[y].push(newnode);
        }
    }

            /*

                    ADD NODE WEIGHT
                    AND EDGE WEIGHT TO 1

             */



    for (y, line) in lines.iter().enumerate()
    {
        for (x, _weight) in line.iter().enumerate()
        {
            if y > 0
            {
                // graph.add_edge(nodes[y - 1][x], nodes[y][x], *weight);
                graph.add_edge(nodes[y - 1][x], nodes[y][x], 0);
            }
            if x > 0
            {
                // graph.add_edge(nodes[y][x - 1], nodes[y][x], *weight);
                graph.add_edge(nodes[y][x - 1], nodes[y][x], 0);
            }
        }
    }

    // println!("{:#?}", graph);

    for line in lines.iter() {
        println!("{:?}", line);
    }

    println!("w:{:?} h:{:?}", width, height);

    let path = astar(&graph, nodes[0][0], |finish| finish == nodes[height - 1][width - 1], |e| *e.weight(), |_| 0);
    println!("weight: {:#?}", path.unwrap().0);
    // println!("ASTAR {:#?}", path);

    // let dijkstra = dijkstra(&graph, nodes[0][0], Some(nodes[height - 1][width - 1]), |e| *e.weight());

    // println!("Dijkstra; {:#?}", dijkstra);

    // let bellman = bellman_ford(&graph, nodes[0][0]);

    // println!("bellman; {:#?}", bellman);


    // 2819 == too low
    // 2845 == too high
    // 2842 == THE RIGHT ANSWER - but solved by asja


    Ok(())
}