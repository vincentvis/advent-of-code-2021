fs = require('fs');

function has_something_to_do(coords)
{
    if (
            (coords[0][0] < 100 && coords[0][1] > 0) &&
            (coords[1][0] < 100 && coords[1][1] > 0) &&
            (coords[2][0] < 100 && coords[2][1] > 0)
        )
    {
        return true;
    }
    return false;
}

let region = [];

fs.readFile('array_maker', 'utf8', function (err,data) {
    let len = data.length;
    for (x = 0; x <= len; x++)
    {
        region[x] = [];
        if (x % 1000 == 0)
            console.log("do ", x);
        for (y = 0; y <= len; y++)
        {
            region[x][y] = data;
        }
    }
    console.log({len});


// fs.readFile('../input', 'utf8', function (err,data) {
// fs.readFile('../very_simple', 'utf8', function (err,data) {
fs.readFile('../simple', 'utf8', function (err,data) {
    let min = 0;
    let max = 0;
    toggles = data.split("\n").filter(Boolean).map(str => {
        let parts = str.split(' ');
        let coords = parts[1].split(",").map(c => {
            let obj = {};
            obj[c[0]] = c.slice(2).split("..").map(s => Number(s))
            return obj
        });

        if (coords[0].x[0] < min) { min = coords[0].x[0] };
        if (coords[0].x[1] < min) { min = coords[0].x[1] };
        if (coords[0].x[0] > max) { max = coords[0].x[0] };
        if (coords[0].x[1] > max) { max = coords[0].x[1] };

        if (coords[1].y[0] < min) { min = coords[1].y[0] };
        if (coords[1].y[1] < min) { min = coords[1].y[1] };
        if (coords[1].y[0] > max) { max = coords[1].y[0] };
        if (coords[1].y[1] > max) { max = coords[1].y[1] };

        if (coords[2].z[0] < min) { min = coords[2].z[0] };
        if (coords[2].z[1] < min) { min = coords[2].z[1] };
        if (coords[2].z[0] > max) { max = coords[2].z[0] };
        if (coords[2].z[1] > max) { max = coords[2].z[1] };

        let cube = [
                        [coords[0].x[0],coords[0].x[1]],
                        [coords[1].y[0],coords[1].y[1]],
                        [coords[2].z[0],coords[2].z[1]]
                ]


        return {on_off: parts[0] == "on", coords: cube}
    });

    console.log({min,max});
    let offset = Math.abs(min);
    toggles = toggles.map(toggle => {
        let new_cube = [
            [toggle.coords[0][0] + offset, toggle.coords[0][1] + offset],
            [toggle.coords[1][0] + offset, toggle.coords[1][1] + offset],
            [toggle.coords[2][0] + offset, toggle.coords[2][1] + offset]
        ]
        return {on_off: toggle.on_off, coords: new_cube}
    })

    range = max - min;
    // region = new Array(range + 1);
    // for (x = 0; x <= range; x++)
    // {
    //     console.log("make array ", x);
    //     region[x] = [];
    //     for (y = 0; y <= range; y++)
    //     {
    //         region[x][y] = [];
    //         for (z = 0; z <= range; z++)
    //             region[x][y][z] = false;
    //     }
    // }

    let total_while_doing = 0;

    toggles.forEach(toggle => {
        let x_start = toggle.coords[0][0];
        let x_end = toggle.coords[0][1];
        let y_start = toggle.coords[1][0];
        let y_end = toggle.coords[1][1];
        let z_start = toggle.coords[2][0];
        let z_end = toggle.coords[2][1];

        if (x_start < 0) x_start = 0;
        if (x_end > range) x_start = range;
        if (y_start < 0) y_start = 0;
        if (y_end > range) y_start = range;
        if (z_start < 0) z_start = 0;
        if (z_end > range) z_start = range;

        console.log("Do: ", {x_start,x_end}, {y_start,y_end}, {z_start,z_end})

        for (let x = x_start; x <= x_end; x++)
        {
            for (let y = y_start; y <= y_end; y++)
            {
                for (let z = z_start; z <= z_end; z++)
                {
                    if (region[x][y][z] === '0' && toggle.on_off === true)
                        total_while_doing += 1;
                    else if (region[x][y][z] === '1' && toggle.on_off === false)
                        total_while_doing -= 1;
                    region[x][y][z] = toggle.on_off === true ? '1' : '0';
                }
            }
        }
    });

    console.log("total while doing ", total_while_doing)

    // let num_on = 0;
    // for (x = 0; x <= range; x++)
    //     for (y = 0; y <= range; y++)
    //         for (z = 0; z <= range; z++)
    //             if (region[x][y][z] === true) num_on++;

    // console.log(num_on);

})

})