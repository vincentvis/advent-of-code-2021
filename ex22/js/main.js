fs = require('fs');

function has_something_to_do(coords)
{
    if (
            (coords[0][0] < 100 && coords[0][1] > 0) &&
            (coords[1][0] < 100 && coords[1][1] > 0) &&
            (coords[2][0] < 100 && coords[2][1] > 0)
        )
    {
        return true;
    }
    return false;
}

fs.readFile('../input', 'utf8', function (err,data) {
// fs.readFile('../very_simple', 'utf8', function (err,data) {
// fs.readFile('../simple', 'utf8', function (err,data) {
    toggles = data.split("\n").filter(Boolean).map(str => {
        let parts = str.split(' ');
        let coords = parts[1].split(",").map(c => {
            let obj = {};
            obj[c[0]] = c.slice(2).split("..").map(s => Number(s))
            return obj
        });

        let cube = [
                        [coords[0].x[0] + 50,coords[0].x[1] + 50],
                        [coords[1].y[0] + 50,coords[1].y[1] + 50],
                        [coords[2].z[0] + 50,coords[2].z[1] + 50]
                ]


        return {on_off: parts[0] == "on", coords: cube}
    });

    region = new Array(101);
    for (x = 0; x <= 100; x++)
    {
        region[x] = new Array(101);
        for (y = 0; y <= 100; y++)
        {
            region[x][y] = new Array(101);
            for (z = 0; z <= 100; z++)
                region[x][y][z] = false;
        }
    }

    todo_part1 = toggles.filter(toggle => has_something_to_do(toggle.coords));

    console.log(todo_part1)

    let total_while_doing = 0;

    todo_part1.forEach(toggle => {
        let x_start = toggle.coords[0][0];
        let x_end = toggle.coords[0][1];
        let y_start = toggle.coords[1][0];
        let y_end = toggle.coords[1][1];
        let z_start = toggle.coords[2][0];
        let z_end = toggle.coords[2][1];

        if (x_start < 0) x_start = 0;
        if (x_end > 100) x_start = 100;
        if (y_start < 0) y_start = 0;
        if (y_end > 100) y_start = 100;
        if (z_start < 0) z_start = 0;
        if (z_end > 100) z_start = 100;

        for (let x = x_start; x <= x_end; x++)
        {
            for (let y = y_start; y <= y_end; y++)
            {
                for (let z = z_start; z <= z_end; z++)
                {
                    if (region[x][y][z] === false && toggle.on_off === true)
                        total_while_doing += 1;
                    else if (region[x][y][z] === true && toggle.on_off === false)
                        total_while_doing -= 1;
                    region[x][y][z] = toggle.on_off;
                }
            }
        }
    });

    console.log("total while doing ", total_while_doing)

    let num_on = 0;
    for (x = 0; x <= 100; x++)
        for (y = 0; y <= 100; y++)
            for (z = 0; z <= 100; z++)
                if (region[x][y][z] === true) num_on++;

    console.log(num_on);

})
