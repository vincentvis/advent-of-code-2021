use std::time::Instant;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::collections::HashMap;


fn mode(numbers: &[i32]) -> Option<i32> {
    let mut counts = HashMap::new();

    numbers.iter().copied().max_by_key(|&n| {
        let count = counts.entry(n).or_insert(0);
        *count += 1;
        *count
    })
}

// fn sumrange(diff: i32) {
//     let mut total: i32 = 0;
//     for i in 0..diff {
//         total = total + i;
//     }
// }

fn main() -> io::Result<()> {
    let now = Instant::now();
    let f = File::open("../input")?;
    // let f = File::open("../example_input")?;
    let reader = BufReader::new(f);
    let mut data: Vec<i32> = Vec::new();

    // reading the file
    for line in reader.lines() {
        data = line.as_ref()
                    .unwrap().split(',')
                    .map(|s| s.parse().unwrap())
                    .collect();
    }

    // sorting it to make the median easier
    data.sort();

    // some data just to have it, most is unused
    let sum: i32 = data.iter().sum();
    let avg: f32 = sum as f32 / data.len() as f32;
    let median: i32 = data[data.len() / 2];
    let mode: i32 = mode(&data).unwrap();

    let now_p1 = Instant::now();
    // part 1 calculations
    let from_median = data.iter().fold(0, |acc, item| acc + (median - item).abs());
    let from_median_min1 = data.iter().fold(0, |acc, item| acc + (median-1 - item).abs());
    let from_median_plus1 = data.iter().fold(0, |acc, item| acc + (median+1 - item).abs());
    let p1_elapsed = now_p1.elapsed();

    let now_p2_with_setup = Instant::now();
    // setup for part 2
    // an array with all the "costs"
    let mut array: [i32; 2000] = [0; 2000];

    array[0] = 0;
    array[1] = 1;
    array[2] = 3;
    for i in 2..2000 {
        array[i] = (array[i - 1] + i as i32) as i32
    }

    // the average but as abs value insteaad of float
    let avg_abs: i32 = avg as i32;

    // search this many around the average
    let range_to_search: i32 = 1;

    // a vector for the results
    let mut results_p2: Vec<i32> = Vec::new();

    let now_p2_without_setup = Instant::now();
    // calculate for around the average
    for num in (avg_abs - range_to_search)..(avg_abs + range_to_search) {
        let result = data.iter().fold(0, |acc, item| {
            acc + array[(num - item).abs() as usize]
        });
        results_p2.push(result);
    }

    let p2_with_setup = now_p2_with_setup.elapsed();
    let p2_without_setup = now_p2_without_setup.elapsed();

    let elapsed_time = now.elapsed();



    // println!("data:\t{:?}", data);
    println!("sum:\t{:?} | avg: {:?} | median: {:?} | mode: {:?} | max: {:?}", sum, avg, median, mode, data.iter().max().unwrap());

    println!("Part1 result: ({:?} > {:?} < {:?})", from_median_plus1, from_median, from_median_min1);
    if from_median_plus1 > from_median && from_median_min1 > from_median
    {
        println!("Pretty sure that {:?} is correct", from_median);
    }

    println!("Part2:");
    println!("Some {:?} around the average {:?}", range_to_search, avg_abs);
    println!("all: {:?}", results_p2);
    println!("result: {:?}", results_p2.iter().min().unwrap());


    println!("\nTo run both part 1 and part 2  {:?}ns ({:?}ms) ", elapsed_time.as_nanos(), elapsed_time.as_nanos() as f64 / 100000.0);
    println!("Part1 took: \t\t\t{:?}ns ({:?}ms)", p1_elapsed.as_nanos(), p1_elapsed.as_nanos() as f64 / 100000.0);
    println!("Part2 with setup took:\t\t{:?}ns ({:?}ms)", p2_with_setup.as_nanos(), p2_with_setup.as_nanos() as f64 / 100000.0);
    println!("Part2 without setup took:\t{:?}ns ({:?}ms)", p2_without_setup.as_nanos(), p2_without_setup.as_nanos() as f64 / 100000.0);




    Ok(())
}