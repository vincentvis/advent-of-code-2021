use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

use std::collections::HashMap;
// use std::collections::HashSet;
//
#[derive(Hash, Eq, PartialEq, Debug)]
pub struct Pair {
    key: String,
    left: String,
    new_char: char,
    right: String,
    new_count: u128,
    old_count: u128,
    is_updated: bool
}

fn main() -> io::Result<()> {
    let f = File::open("../input")?;
    // let f = File::open("../simple")?;
    let mut reader = BufReader::new(f);
    let mut input = String::new();

    let _input_len = reader.read_line(&mut input);
    input = (&input[0..input.len()-1]).to_string();

    /*
        a vector containing the mutable data
        a hashmap containing the index of the "AB" in the vector
     */

    // let mut counts: Vec<(i32, i32)> = Vec::new();

    // let mut left_pairs = HashMap::new();
    // let mut right_pairs = HashMap::new();

    let mut pairs = HashMap::new();
    let mut pair_strs: Vec<String> = Vec::new();
    let mut char_count = HashMap::new();

    // let mut counts = HashMap::new();

    for line in reader.lines() {
        if line.as_ref().unwrap().len() == 0
        {
            continue ;
        }
        let self_vec: Vec<char> = vec![line.as_ref().unwrap().chars().nth(0).unwrap(), line.as_ref().unwrap().chars().nth(1).unwrap()];
        let a_vec: Vec<char> = vec![line.as_ref().unwrap().chars().nth(0).unwrap(), line.as_ref().unwrap().chars().nth(6).unwrap()];
        let b_vec: Vec<char> = vec![line.as_ref().unwrap().chars().nth(6).unwrap(), line.as_ref().unwrap().chars().nth(1).unwrap()];
        let self_str: String = self_vec.into_iter().collect();

        if ! char_count.contains_key(&line.as_ref().unwrap().chars().nth(0).unwrap())
        {
            char_count.insert(line.as_ref().unwrap().chars().nth(0).unwrap(), 0 as u128);
        }
        if ! char_count.contains_key(&line.as_ref().unwrap().chars().nth(1).unwrap())
        {
            char_count.insert(line.as_ref().unwrap().chars().nth(1).unwrap(), 0 as u128);
        }
        if ! char_count.contains_key(&line.as_ref().unwrap().chars().nth(6).unwrap())
        {
            char_count.insert(line.as_ref().unwrap().chars().nth(6).unwrap(), 0 as u128);
        }

        pair_strs.push(self_str.clone());
        pairs.insert(self_str.clone(), Pair {
            key: self_str.clone(),
            left: a_vec.into_iter().collect(),
            right: b_vec.into_iter().collect(),
            new_char: line.as_ref().unwrap().chars().nth(6).unwrap(),
            old_count: 0 as u128,
            new_count: 0 as u128,
            is_updated: false
        });


        // counts.push((0, 0));
    }

    // set initial count
    *char_count.get_mut(&input.chars().nth(0).unwrap()).unwrap() += 1;
    for n in 1..input.len()
    {
        let two_chars = vec![input.chars().nth(n - 1).unwrap(), input.chars().nth(n).unwrap()];
        let hash_pair = pairs.get_mut(&two_chars.into_iter().collect::<String>());
        let data = hash_pair.unwrap();
        data.old_count += 1;
        *char_count.get_mut(&input.chars().nth(n).unwrap()).unwrap() += 1;
    }

    for i in 0..80
    {
        for pair_str in pair_strs.iter() {
            let mut data = pairs.get_mut(pair_str).unwrap();
            let count = data.old_count;
            if count != 0
            {
                let (left_var, right_var) = (data.left.clone(), data.right.clone());
                // LEFT
                data = pairs.get_mut(&left_var).unwrap();
                data.new_count += count;
                data.is_updated = true;

                // RIGHT
                data = pairs.get_mut(&right_var).unwrap();
                data.new_count += count;
                data.is_updated = true;

                // get current str again
                data = pairs.get_mut(pair_str).unwrap();
                // get char counter
                let charcounter = char_count.get_mut(&data.new_char).unwrap();
                *charcounter += count as u128;
                data.old_count -= count;
            }
        }
        for pair_str in pair_strs.iter() {
            let data = pairs.get_mut(pair_str).unwrap();
            if data.is_updated == true
            {
                data.old_count = data.new_count;
                data.new_count = 0;
                data.is_updated = false;
            }
        }
        if i == 9
        {
            let gets_max = char_count.iter().max_by_key(|entry | entry.1).unwrap();
            let gets_min = char_count.iter().min_by_key(|entry | entry.1).unwrap();
            println!("After 10 iterations: {:?}", gets_max.1 - gets_min.1);
        }
        if i == 39
        {
            let gets_max = char_count.iter().max_by_key(|entry | entry.1).unwrap();
            let gets_min = char_count.iter().min_by_key(|entry | entry.1).unwrap();
            println!("After 40 iterations: {:?}", gets_max.1 - gets_min.1);
        }
    }


    // println!("{:#?}", char_count);
    let gets_max = char_count.iter().max_by_key(|entry | entry.1).unwrap();
    let gets_min = char_count.iter().min_by_key(|entry | entry.1).unwrap();

    println!("After 80 iterations: {:?} ({:?} - {:?})", gets_max.1 - gets_min.1, gets_max.1, gets_min.1);

    Ok(())
}