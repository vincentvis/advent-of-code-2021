// use std::collections::VecDeque;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;



fn recursion(mut line: String) -> String
{
	let initial_length = line.len();
	line = line.split("[]").collect::<String>();
	line = line.split("{}").collect::<String>();
	line = line.split("<>").collect::<String>();
	line = line.split("()").collect::<String>();
	if initial_length == line.len()
	{
		return line;
	}
	return recursion(line);
}


fn main() -> io::Result<()> {
	let f = File::open("../example_input")?;
	// let f = File::open("../input")?;
	let reader = BufReader::new(f);
	// let mut raw_data: Vec<String> = Vec::new();

	// let mut array: [char; 127] = ['\0'; 127];
	// array['{' as usize] = '}';
	// array['<' as usize] = '>';
	// array['[' as usize] = ']';
	// array['(' as usize] = ')';

	// let mut scores_p1: [i32; 127] = [0; 127];
	// scores_p1[')' as usize] = 3;
	// scores_p1[']' as usize] = 57;
	// scores_p1['}' as usize] = 1197;
	// scores_p1['>' as usize] = 25137;

	// let mut scores_p2: [i64; 127] = [0; 127];
	// scores_p2['(' as usize] = 1;
	// scores_p2['[' as usize] = 2;
	// scores_p2['{' as usize] = 3;
	// scores_p2['<' as usize] = 4;

	// let mut total_score_p1: i32 = 0;
	// let mut total_score_p2: Vec<i64> = Vec::new();

	// reading the file
	for line in reader.lines() {
		let mut currentline = line.unwrap();

		println!("{:?}", currentline);
		currentline = recursion(currentline);
		println!("{:?}", currentline);

		// let mut queue: VecDeque<char> = VecDeque::new();
		// let mut chars = currentline.chars();
		// for _c in 0..currentline.len()
		// {
		// 	let current = chars.next().unwrap();
		// 	if current == '{' || current == '[' || current == '<' || current == '('
		// 	{
		// 		queue.push_back(current);
		// 	}
		// 	else {
		// 		if queue.is_empty()
		// 		{
		// 			println!("Problem found on {:?} char {:?}", currentline, current);
		// 			break ;
		// 		}
		// 		let prev_char = queue.pop_back().unwrap();
		// 		let matching_char = array[prev_char as usize];
		// 		if current != matching_char
		// 		{
		// 			// println!("{:?} - Expected {:?}, but found {:?} instead", currentline, matching_char, current);
		// 			total_score_p1 = total_score_p1 + scores_p1[current as usize];
		// 			queue.clear();
		// 			break ;
		// 		}
		// 	}
		// }
		// if ! queue.is_empty()
		// {
		// 	let mut total: i64 = 0;
		// 	while ! queue.is_empty()
		// 	{
		// 		let elem = queue.pop_back().unwrap();
		// 		total = total * 5;
		// 		total = total + scores_p2[elem as usize];
		// 	}
		// 	total_score_p2.push(total);
		// }
	}



	// let mut chars = raw_data[0].chars();
	// println!("{:?}", chars);
	// queue.push_back(chars.next());
	// queue.push_back(chars.next());
	// if char is "[{<(" => open
	// println!("p1:{:?}", total_score_p1);
	// total_score_p2.sort();
	// println!("p2:{:?}", total_score_p2[total_score_p2.len() / 2]);
	// println!("q:{:?}", queue);

	Ok(())
}
