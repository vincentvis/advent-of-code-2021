use std::collections::VecDeque;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

#[derive(Debug)]
struct Coord {
	x: usize,
	y: usize
}

fn flood_size(start: &Coord, mut map: Vec<Vec<u8>>) -> i32 {
	let mut size: i32 = 1;
	map[start.y][start.x] = 10;
	let width = map[0].len();
	let height = map.len();

	// println!("start: {:?}", start);
	// println!("{:?}", map);
	// for h in 0..height {
	// 	for w in 0..width {
	// 		if map[h][w] == 10
	// 		{
	// 			print!(".");
	// 		}
	// 		else {
	// 			print!("{:?}", map[h][w]);
	// 		}
	// 	}
	// 	print!("\n");
	// }


	let mut queue: VecDeque<Coord> = VecDeque::new();
	if start.y > 0 && map[start.y - 1][start.x] < 9
	{
		queue.push_back(Coord { x: start.x, y: start.y - 1 });
		map[start.y - 1][start.x] = 10;
		size = size + 1;
	}
	if start.x > 0 && map[start.y][start.x - 1] < 9
	{
		queue.push_back(Coord { x: start.x - 1, y: start.y });
		map[start.y][start.x - 1] = 10;
		size = size + 1;
	}
	if start.y < height - 1 && map[start.y + 1][start.x] < 9
	{
		queue.push_back(Coord { x: start.x, y: start.y + 1 });
		map[start.y + 1][start.x] = 10;
		size = size + 1;
	}
	if start.x < width - 1 && map[start.y][start.x + 1] < 9
	{
		queue.push_back(Coord { x: start.x + 1, y: start.y });
		map[start.y][start.x + 1] = 10;
		size = size + 1;
	}

	// println!("queue {:?}", queue);

	// let mut iterator = queue.iter();

	while ! queue.is_empty()
	{
		let elem = queue.pop_front().unwrap();
		if elem.x > 0 && map[elem.y][elem.x - 1] < 9
		{
			queue.push_back(Coord {y: elem.y, x: elem.x - 1});
			map[elem.y][elem.x - 1] = 10;
			size = size + 1;
		}
		if elem.y > 0 && map[elem.y - 1][elem.x] < 9
		{
			queue.push_back(Coord {y: elem.y - 1, x: elem.x});
			map[elem.y - 1][elem.x] = 10;
			size = size + 1;
		}
		if elem.x < width - 1 && map[elem.y][elem.x + 1] < 9
		{
			queue.push_back(Coord {y: elem.y, x: elem.x + 1});
			map[elem.y][elem.x + 1] = 10;
			size = size + 1;
		}
		if elem.y < height - 1 && map[elem.y + 1][elem.x] < 9
		{
			queue.push_back(Coord {y: elem.y + 1, x: elem.x});
			map[elem.y + 1][elem.x] = 10;
			size = size + 1;
		}
	}
	// println!("found {:?}", size);

	// for h in 0..height {
	// 	for w in 0..width {
	// 		if map[h][w] == 10
	// 		{
	// 			print!(".");
	// 		}
	// 		else {
	// 			print!("{:?}", map[h][w]);
	// 		}
	// 	}
	// 	print!("\n");
	// }
	// println!("\n");

	return size;
}

fn main() -> io::Result<()> {
	// let f = File::open("../example_input")?;
	let f = File::open("../input")?;
	let reader = BufReader::new(f);
	let mut raw_data: Vec<Vec<u8>> = Vec::new();

	// reading the file
	for line in reader.lines() {
		/*raw_data.push(line.unwrap()			// unwrap the value, because it is a "result" whatever that means
							.chars()		// make it into a char iteratable
							.map(|c| {		// map over it
								c.to_string()	// convert it to a string, because parse expects a string
								 .parse()		// parse to number
								 .unwrap()		// unwrap result
								})
							.collect::<Vec<u8>>()); // collect in a vector */
		// a "better" way, because it's not turned into a string first.. since everything is 1 digit
		raw_data.push(line.unwrap()			// unwrap the value, because it is a "result" whatever that means
							.chars()		// make it into a char iteratable
							.map(|c| {		// map over it
								c.to_digit(10)		// use to_ditit on the char directly
								 .unwrap() as u8	// unwrap result and "cast" to u8
								})
							.collect::<Vec<u8>>()); // collect in a vector
	}


	let width = raw_data[0].len();
	let height = raw_data.len();

	// prints the original map
	// for h in 0..height {
	// 	println!("{:?}{:?}{:?}{:?}{:?}{:?}{:?}{:?}{:?}{:?}",
	// 		raw_data[h][0], raw_data[h][1], raw_data[h][2], raw_data[h][3], raw_data[h][4],
	// 		raw_data[h][5], raw_data[h][6], raw_data[h][7], raw_data[h][8], raw_data[h][9]);
	// }
	// println!("\n");


	let mut low_points: Vec<Coord> = Vec::new();
	let mut risk_level: i32 = 0;

	for h in 0..height {
		for w in 0..width {
			let mut needs = 4;
			let mut meets = 0;
			if h == 0 {
				needs = needs - 1;
			}
			else {
				if raw_data[h - 1][w] > raw_data[h][w]
				{
					meets = meets + 1;
				}
			}
			if h == height-1 {
				needs = needs - 1;
			}
			else {
				if raw_data[h + 1][w] > raw_data[h][w]
				{
					meets = meets + 1;
				}
			}
			if w == 0
			{
				needs = needs - 1;
			}
			else {
				if raw_data[h][w - 1] > raw_data[h][w]
				{
					meets = meets + 1;
				}
			}
			if w == width-1
			{
				needs = needs - 1;
			}
			else {
				if raw_data[h][w + 1] > raw_data[h][w]
				{
					meets = meets + 1;
				}
			}

			if needs == meets
			{
				low_points.push(
						Coord {
							x: w,
							y: h
						});
				risk_level = risk_level + raw_data[h][w] as i32 + 1;
				// print!("{:?}", raw_data[h][w]);
			}
			else {
				// print!(".");
			}
		}
		// println!(" ");
	}

	println!("Risk level: {:?}", risk_level);
	// println!("LowPoints({:?}): {:?}", low_points.len(), low_points);

	let mut basin_sizes: Vec<i32> = Vec::new();

	let num_low_points = low_points.len();
	for i in 0..num_low_points {
		basin_sizes.push(flood_size(low_points.get(i).unwrap(), raw_data.clone()));
	}

	basin_sizes.sort();
	let num_basins = basin_sizes.iter().len();
	println!("{:?} * {:?} * {:?} = {:?} ", basin_sizes[num_basins - 1], basin_sizes[num_basins - 2], basin_sizes[num_basins - 3],
		basin_sizes[num_basins - 1] * basin_sizes[num_basins - 2] * basin_sizes[num_basins - 3]);



	Ok(())
}