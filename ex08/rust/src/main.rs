use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
	let f = File::open("../input")?;
	// let f = File::open("../medium_input")?;
	// let f = File::open("../example_input")?;
	let reader = BufReader::new(f);
	let mut raw_data: Vec<String> = Vec::new();

	// reading the file
	for line in reader.lines() {
		raw_data.push(line.unwrap());
	}

	// first 10 elements will be before the | last 4 elems afteer it
	let data: Vec<Vec<&str>> = raw_data.iter().map(|s| s.split(' ').collect::<Vec<&str>>()).collect();

	let count_1478: i32 = data.iter().map(|a| {
		let mut inner_count = 0;
		if a[11].len() == 2 || a[11].len() == 4 || a[11].len() == 3 || a[11].len() == 7
		{
			inner_count = inner_count + 1;
		}
		if a[12].len() == 2 || a[12].len() == 4 || a[12].len() == 3 || a[12].len() == 7
		{
			inner_count = inner_count + 1;
		}
		if a[13].len() == 2 || a[13].len() == 4 || a[13].len() == 3 || a[13].len() == 7
		{
			inner_count = inner_count + 1;
		}
		if a[14].len() == 2 || a[14].len() == 4 || a[14].len() == 3 || a[14].len() == 7
		{
			inner_count = inner_count + 1;
		}
		return inner_count;
	}).sum();

	println!("Count all 1478s: {:?}", count_1478);


	let characters = [
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g'],
		vec!['a','b','c','d','e','f','g']
	];

	println!("{:?}", data[0]);
	println!("{:?}", characters);


	Ok(())
}
