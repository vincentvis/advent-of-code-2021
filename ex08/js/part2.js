fs = require('fs');

const SORTED_INDEX_1 = 0;
const RESULT_INDEX_1 = 1;
const SORTED_INDEX_4 = 2;
const RESULT_INDEX_4 = 4;
const SORTED_INDEX_7 = 1;
const RESULT_INDEX_7 = 7;
const SORTED_INDEX_8 = 9;
const RESULT_INDEX_8 = 8;

fs.readFile('../input', 'utf8', function (err,data) {
	if (err) {
		return console.log(err);
	}
	let lines = data.trim().split('\n');
	let output_values = lines.map(line => line.split(" | ")[1].split(' '));
	let signal_patterns = lines.map(
			line => {
				let arr = line.split(" | ")[0]
									.split(' ')
									.sort((e1,e2) => e1.length - e2.length)
									.map(str => str.split('').sort((a,b) => a.charCodeAt() - b.charCodeAt()).join(''));
				let outp = line.split(" | ")[1]
									.split(' ')
									.map(str => str.split('').sort((a,b) => a.charCodeAt() - b.charCodeAt()).join(''));
				return {arr, outp};
			}
		).map(d => {
			let arr = d.arr;
			let known = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

			// these are known just based on their length
			known[RESULT_INDEX_1] = arr[SORTED_INDEX_1];
			known[RESULT_INDEX_4] = arr[SORTED_INDEX_4];
			known[RESULT_INDEX_7] = arr[SORTED_INDEX_7];
			known[RESULT_INDEX_8] = arr[SORTED_INDEX_8];

			// groups of 5 and 6 length for later convencience
			let len5 = arr.filter(el=>el.length == 5);
			let len6 = arr.filter(el=>el.length == 6);
			// array of chars in 4
			let four = known[RESULT_INDEX_4].split('');
			let one = known[RESULT_INDEX_1].split('');

			// 9 is known just based on that it's the only one that fully
			// overlaps with 4
			known[9] = len6.filter(str => {
				return (
					str.indexOf(four[0]) != -1 &&
					str.indexOf(four[1]) != -1 &&
					str.indexOf(four[2]) != -1 &&
					str.indexOf(four[3]) != -1
				)
			})[0];

			// 0 and 6 should be known based in 1..
			// assuming we remove 9 from the len6 array
			let len6_no9 = len6.filter(str => str != known[9]);

			// so we know 0 now based on 1
			known[0] = len6_no9.filter(str => {
				return (
					str.indexOf(one[0]) != -1 &&
					str.indexOf(one[1]) != -1
				)
			})[0];
			// and 6 is the other one
			known[6] = len6_no9.filter(str => str != known[0])[0];


			// we can differentiate 3 from 2 and 5 by checking for CF as well
			known[3] = len5.filter(str => {
				return (
					str.indexOf(one[0]) != -1 &&
					str.indexOf(one[1]) != -1
				)
			})[0];

			// remove 3 from the length 5 array
			let len5_no3 = len5.filter(str => str != known[3]);

			// have 6 as chars
			let six = known[6].split('');
			// 2 is 3 "different" from 6 but 5 is only 2 different!
			known[2] = len5_no3.filter(str => {
				let count = 0;
				six.forEach(c => {
					if (str.indexOf(c) != -1)
						count++;
				})
				return count == 4;
			})[0];

			known[5] = len5_no3.filter(str => str != known[2])[0];

			let result = Number(d.outp.map(el => {
				return known.indexOf(el).toString()
			}).join(''));

			return {known, result};
		});

	console.log(signal_patterns.reduce((acc, item) => acc + item.result, 0));




});

