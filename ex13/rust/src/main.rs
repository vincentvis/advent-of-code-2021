use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn show_grid(grid: &Vec<Vec<i32>>, height: usize, width: usize)
{
    let mut dots = 0;
    // println!("    0123456789a");
    for row in 0..height {
        print!("{:2?}: ", row);
        for col in 0..width {
            if grid[row][col] == 0
            {
                print!(".");
            }
            else if grid[row][col] == -1
            {
                print!("-");
            }
            else if grid[row][col] == -2
            {
                print!("|");
            }
            else if grid[row][col] == -4
            {
                print!("^");
            }
            else if grid[row][col] == -5
            {
                print!("<");
            }
            else {
                // print!("{:?}", grid[row][col]);
                print!("#");
                dots = dots + 1;
            }
        }
        print!("\n");
    }
    println!("Dots visible: {:?}", dots);
}

// index - (2 * i + 2) = fold up index

fn fold_grid(grid: &mut Vec<Vec<i32>>, direction: char, index: usize, width: usize, height: usize)
{
    if direction == 'y'
    {
        for row in (index + 1)..height
        {
            let sub = 2 * (row - index);
            if sub > row
            {
                break ;
            }
            for x in 0..width {
                grid[row - sub][x] = grid[row - sub][x] + grid[row][x];
                grid[row][x] = 0;
            }
        }
        // for col in 0..width {
        //     grid[index][col] = -1;
        // }
    }
    else {
        for col in (index + 1)..width
        {
            let sub = 2 * (col - index);
            if sub > col
            {
                break ;
            }
            for y in 0..height {
                grid[y][col - sub] = grid[y][col - sub] + grid[y][col];
                grid[y][col] = 0;
            }
        }
        // for row in 0..height {
        //     grid[row][index] = -2;
        // }
    }
}

fn main() -> io::Result<()> {
    let f = File::open("../input")?;
    // let f = File::open("../example")?;
    let reader = BufReader::new(f);

    let mut coords: Vec<(usize,usize)> = Vec::new();
    let mut folds: Vec<(char,usize)> = Vec::new();

    for line in reader.lines() {
        let data = line.unwrap();
        if data.len() < 1
        {
            continue ;
        }
        if ! data.starts_with("fold")
        {
            let c = data.split(',').collect::<Vec<&str>>().iter().map(|s| s.parse::<usize>().unwrap()).collect::<Vec<usize>>();
            coords.push((c[0],c[1]));
        }
        else {
            let c = data.split('=').collect::<Vec<&str>>();
            folds.push((c[0].chars().last().unwrap(), c[1].parse::<usize>().unwrap()));
        }
    }

    let width = coords.iter().map(|n| n.0).max().unwrap() + 1;
    let height = coords.iter().map(|n| n.1).max().unwrap() + 1;

    let mut grid = vec![vec![0; width + 1]; height + 1];

    for coord in coords.iter() {
        grid[coord.1][coord.0] = grid[coord.1][coord.0] + 1;
    }

    for fold in folds.iter() {
        fold_grid(&mut grid, fold.0, fold.1, width, height);
    }
    show_grid(&grid, height, width);


    // println!("fold {:?}", folds);

    Ok(())
}
