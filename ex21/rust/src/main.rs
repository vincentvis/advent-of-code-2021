use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Player {
    id: usize,
    start_position: usize,
    current_position: usize,
    score: usize
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct RollOption {
    sum: usize,
    universes_created: usize,
    new_positions: [usize; 11],
    next: Vec<[RollOption; 7]>
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct PlayerCounter {
    position: usize,
    score: usize,
    amount_p0: [usize; 10],
    amount_p1: [usize; 10]
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Step {
    position: usize,
    score: usize,
    universes: usize,
    rolled: usize,
    step: usize,
    next: Vec<Step>
}

impl Player {
    fn create_player(id: usize, start_position: usize) -> Player {
        Player { id: id, start_position: start_position, current_position: start_position, score: 0 }
    }

    // fn update_score(&mut self, score: usize)
    // {
    //     self.score += score;
    //     self.current_position = score;
    // }
}

// fn index_from_position(pos: usize) -> usize
// {
//     return (pos - 1) * 30;
// }

const NUM_POSITIONS: usize = 10;
const NUM_SUMS: usize = 7;

// fn roll_for_step(
//         current_step: Step,
//         rolls: [(usize,
//         usize); 7],
//         new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS],
//         step: usize,
//         step_wins_in_universes: &mut [usize; 10]
//     ) -> Vec<Step> {
//     let mut new_steps: Vec<Step> = Vec::new();

//     for roll in rolls.iter() {
//         new_steps.push(Step {
//             position: new_positions[roll.0 - 3][current_step.position],
//             score: current_step.score + new_positions[roll.0 - 3][current_step.position],
//             universes: current_step.universes * (roll.1 * 3),
//             step: step,
//             rolled: roll.0,
//             next: Vec::new()
//         });

//         if new_steps[roll.0 - 3].score < 21
//         {
//             let clone = new_steps[roll.0 - 3].clone();
//             new_steps[roll.0 - 3].next = roll_for_step(clone, rolls, new_positions, step + 1, step_wins_in_universes);
//         }
//         else {
//             step_wins_in_universes[step - 1] += new_steps[roll.0 - 3].universes;
//         }
//     }
//     return new_steps;
// }

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct PlayerState {
    player: usize,
    position: usize,
    rolled: usize,
    universes: usize,
    step: usize,
    score: usize,
    next: Vec<PlayerState>
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct PlayerStateCopy {
    player: usize,
    position: usize,
    rolled: usize,
    universes: usize,
    step: usize,
    score: usize,
}


// fn next_player_index(current: usize) -> usize {
//     if current == 1
//     {
//         return 1;
//     }
//     return 0;
// }

// fn simulate_game(prev_state: PlayerState, rolls: [(usize, usize); 7], new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS], step: usize)
// {
//     for roll in rolls.iter() {
//         start.next.push(PlayerState {
//             player: start.player,
//             position: new_positions[roll.0 - 3][start.position],
//             rolled: roll.0,
//             universes: start.universes * roll.1,
//             step: 1,
//             next: Vec::new()
//         });
//     }
// }


fn init_player2(rolls: [(usize, usize); 7], new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS], player_id: usize, player_start: usize, prev_universes: usize) -> Vec<PlayerState>
{
    let mut newsteps: Vec<PlayerState> = Vec::with_capacity(7);

    for roll in rolls.iter()
    {
        newsteps.push(PlayerState {
            player: player_id,
            position: new_positions[roll.0 - 3][player_start],
            rolled: roll.0,
            universes: prev_universes * roll.1,
            step: 1,
            score: new_positions[roll.0 - 3][player_start],
            next: Vec::new()
        });
    }

    return newsteps;
}

fn roll_for_player2(p2: PlayerStateCopy, rolls: [(usize, usize); 7], new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS]) -> Vec<PlayerState>
{
    let mut p2_newsteps: Vec<PlayerState> = Vec::with_capacity(7);

    for roll in rolls {
        p2_newsteps.push(PlayerState {
            player: 2,
            position: new_positions[roll.0 - 3][p2.position],
            rolled: roll.0,
            universes: p2.universes * roll.1,
            step: p2.step + 1,
            score: p2.score + new_positions[roll.0 - 3][p2.position],
            next: Vec::new()
        })
    }

    return p2_newsteps;
}

fn next_two_rolls(p1: PlayerStateCopy, p2: PlayerStateCopy, rolls: [(usize, usize); 7], new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS], total: &mut usize) -> Vec<PlayerState>
{
    let mut p1_newsteps: Vec<PlayerState> = Vec::with_capacity(7);

    println!("total: {:?}", total);

    for roll in rolls {
        p1_newsteps.push(PlayerState {
            player: 1,
            position: new_positions[roll.0 - 3][p1.position],
            rolled: roll.0,
            universes: p1.universes * roll.1,
            step: p1.step + 1,
            score: p1.score + new_positions[roll.0 - 3][p1.position],
            next: Vec::new()
        });

        if p1_newsteps[roll.0 - 3].score < 21
        {
            p1_newsteps[roll.0 - 3].next = roll_for_player2(p2, rolls, new_positions);
        }
        else {
            *total += p1_newsteps[roll.0 - 3].universes;
        }
    }

    for p1_index in 0..7
    {
        if p1_newsteps[p1_index].score < 21 && p1_newsteps[p1_index].step < 5
        {
            for p2_index in 0..7
            {
                if p1_newsteps[p1_index].next[p2_index].score < 21 && p1_newsteps[p1_index].next[p2_index].step < 5
                {
                    p1_newsteps[p1_index].next[p2_index].next = next_two_rolls(
                                    PlayerStateCopy {
                                        player: p1_newsteps[p1_index].player,
                                        position: p1_newsteps[p1_index].position,
                                        rolled: p1_newsteps[p1_index].rolled,
                                        universes: p1_newsteps[p1_index].universes,
                                        step: p1_newsteps[p1_index].step,
                                        score: p1_newsteps[p1_index].score,
                                    },
                                    PlayerStateCopy {
                                        player: p1_newsteps[p1_index].next[p2_index].player,
                                        position: p1_newsteps[p1_index].next[p2_index].position,
                                        rolled: p1_newsteps[p1_index].next[p2_index].rolled,
                                        universes: p1_newsteps[p1_index].next[p2_index].universes,
                                        step: p1_newsteps[p1_index].next[p2_index].step,
                                        score: p1_newsteps[p1_index].next[p2_index].score,
                                    },
                                    rolls,
                                    new_positions,
                                    total
                                );
                }
            }
        }
    }

    return p1_newsteps;
}

fn main() -> io::Result<()> {
    // let f = File::open("../input")?;
    let f = File::open("../simple")?;
    let reader = BufReader::new(f);
    let mut players: Vec<Player> = Vec::new();

    for line in reader.lines() {
        let buffer = line.unwrap();
        let mut parts = buffer.split(' ');
        players.push(Player::create_player(
                parts.nth(1).unwrap().parse().unwrap(),
                parts.nth(2).unwrap().parse().unwrap()
            ));
    }

    println!("PART 2");

    let rolls: [(usize, usize); 7] = [
        (3, 3),/*1),*/
        (4, 9),/*3),*/
        (5, 18),/*6),*/
        (6, 21),/*7),*/
        (7, 18),/*6),*/
        (8, 9),/*3),*/
        (9, 3)/*1) */
    ];

    let new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS] = [
        [0,4,5,6,7,8,9,10,1,2,3],
        [0,5,6,7,8,9,10,1,2,3,4],
        [0,6,7,8,9,10,1,2,3,4,5],
        [0,7,8,9,10,1,2,3,4,5,6],
        [0,8,9,10,1,2,3,4,5,6,7],
        [0,9,10,1,2,3,4,5,6,7,8],
        [0,10,1,2,3,4,5,6,7,8,9],
    ];

    // let mut start: PlayerState = PlayerState {
    //     player: players.get(0).unwrap().id,
    //     position: players.get(0).unwrap().start_position,
    //     rolled: 0,
    //     universes: 1,
    //     step: 0,
    //     score: 0,
    //     next: Vec::new()
    // };

    let mut start: Vec<PlayerState> = Vec::with_capacity(7);

    let mut total: usize = 0;

    for roll in rolls.iter() {
        start.push(PlayerState {
            player: 1,
            position: new_positions[roll.0 - 3][players.get(0).unwrap().start_position],
            score: new_positions[roll.0 - 3][players.get(0).unwrap().start_position],
            rolled: roll.0,
            universes: roll.1,
            step: 1,
            next: Vec::new()
        });

        start[roll.0 - 3].next = init_player2(rolls, new_positions, players.get(1).unwrap().id, players.get(1).unwrap().start_position, start[roll.0 - 3].universes);
    }

    for p1 in 0..7
    {
        for p2 in 0..7
        {
            start[p1].next[p2].next = next_two_rolls(
                                PlayerStateCopy {
                                    player: start[p1].player,
                                    position: start[p1].position,
                                    rolled: start[p1].rolled,
                                    universes: start[p1].universes,
                                    step: start[p1].step,
                                    score: start[p1].score,
                                },
                                PlayerStateCopy {
                                    player: start[p1].next[p2].player,
                                    position: start[p1].next[p2].position,
                                    rolled: start[p1].next[p2].rolled,
                                    universes: start[p1].next[p2].universes,
                                    step: start[p1].next[p2].step,
                                    score: start[p1].next[p2].score,
                                },
                                rolls,
                                new_positions,
                                &mut total
                            );
        }
    }

    // println!("{:#?}", start);


    // let mut start_state_p1: Step = Step {
    //         position: 4,
    //         score: 0,
    //         universes: 1,
    //         step: 0,
    //         rolled: 0,
    //         next: Vec::new()
    //     };
    // let mut start_state_p2: Step = Step {
    //         position: 8,
    //         score: 0,
    //         universes: 1,
    //         step: 0,
    //         rolled: 0,
    //         next: Vec::new()
    //     };

    // let mut step_wins_in_universes_p1: [usize; 10] = [0; 10];
    // let mut step_wins_in_universes_p2: [usize; 10] = [0; 10];

    // start_state_p1.next = roll_for_step(start_state_p1.clone(), rolls.clone(), new_positions.clone(), 1, &mut step_wins_in_universes_p1);
    // start_state_p2.next = roll_for_step(start_state_p2.clone(), rolls.clone(), new_positions.clone(), 1, &mut step_wins_in_universes_p2);
    // println!("{:#?}", start_state_p1);

    // println!("{:?}", step_wins_in_universes_p1);
    // println!("{:?}", step_wins_in_universes_p2);

    // let mut accumul_p1 = step_wins_in_universes_p1.clone();
    // let mut accumul_p2 = step_wins_in_universes_p2.clone();

    // let order: [usize; 7] = [9,8,7,6,5,4,3];
    // for index in order.iter() {
    //     accumul_p1[index - 1] += accumul_p1[*index];
    //     accumul_p2[index - 1] += accumul_p2[*index];
    // }

    // println!("p1: {:?}", accumul_p1);
    // println!("p2: {:?}", accumul_p2);


    Ok(())
}