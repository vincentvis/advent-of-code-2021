use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Player {
    id: usize,
    start_position: usize,
    current_position: usize,
    score: usize
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
struct RollOption {
    sum: usize,
    universes_created: usize,
    new_positions: [usize; 11],
    next: Vec<[RollOption; 7]>
}

#[derive(Hash, Eq, PartialEq, Debug, Clone, Copy)]
pub struct PlayerCounter {
    position: usize,
    score: usize,
    amount_p0: [usize; 10],
    amount_p1: [usize; 10]
}

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Step {
    position: usize,
    score: usize,
    universes: usize,
    rolled: usize,
    step: usize,
    next: Vec<Step>
}

impl Player {
    fn create_player(id: usize, start_position: usize) -> Player {
        Player { id: id, start_position: start_position, current_position: start_position, score: 0 }
    }

    fn update_score(&mut self, score: usize)
    {
        self.score += score;
        self.current_position = score;
    }
}

// fn index_from_position(pos: usize) -> usize
// {
//     return (pos - 1) * 30;
// }

const NUM_POSITIONS: usize = 10;
const NUM_SUMS: usize = 7;

fn roll_for_step(
        current_step: Step,
        rolls: [(usize,
        usize); 7],
        new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS],
        step: usize,
        step_wins_in_universes: &mut [usize; 10]
    ) -> Vec<Step> {
    let mut new_steps: Vec<Step> = Vec::new();

    for roll in rolls.iter() {
        new_steps.push(Step {
            position: new_positions[roll.0 - 3][current_step.position],
            score: current_step.score + new_positions[roll.0 - 3][current_step.position],
            universes: current_step.universes * (roll.1 * 3),
            step: step,
            rolled: roll.0,
            next: Vec::new()
        });

        if new_steps[roll.0 - 3].score < 21
        {
            let clone = new_steps[roll.0 - 3].clone();
            new_steps[roll.0 - 3].next = roll_for_step(clone, rolls, new_positions, step + 1, step_wins_in_universes);
        }
        else {
            step_wins_in_universes[step - 1] += new_steps[roll.0 - 3].universes;
        }
    }
    return new_steps;
}

fn main() -> io::Result<()> {
    // let f = File::open("../input")?;
    let f = File::open("../simple")?;
    let reader = BufReader::new(f);
    let mut players: Vec<Player> = Vec::new();

    for line in reader.lines() {
        let buffer = line.unwrap();
        let mut parts = buffer.split(' ');
        players.push(Player::create_player(
                parts.nth(1).unwrap().parse().unwrap(),
                parts.nth(2).unwrap().parse().unwrap()
            ));
    }

    {
        let _p2_players = players.clone();

        let deterministic_dice_start_value: usize = 1;
        let deterministic_dice_max_value: usize = 100;
        let mut deterministic_dice_value: usize = deterministic_dice_start_value;
        let mut number_of_rolls: usize = 0;
        let number_of_rolls_per_turn = 3;

        let num_players = players.len();

        while players.iter().filter(|p| p.score >= 1000).count() == 0
        {
            for index in 0..num_players {
                let mut roll_sum = deterministic_dice_value * number_of_rolls_per_turn + number_of_rolls_per_turn;
                deterministic_dice_value += number_of_rolls_per_turn;
                if deterministic_dice_value > deterministic_dice_max_value
                {
                    let sub = (deterministic_dice_value % deterministic_dice_max_value) - 1;
                    roll_sum -= sub * 100;
                    deterministic_dice_value = sub + 1;
                }
                let mut new_position = (players[index].current_position + roll_sum) % 10;
                if new_position == 0
                {
                    new_position = 10;
                }
                players[index].update_score(new_position);
                number_of_rolls += number_of_rolls_per_turn;
                if players[index].score >= 1000
                {
                    break ;
                }
            }
        }

        println!("Part 1\n----------------");
        println!("Num rolls: {:?}", number_of_rolls);
        println!("Players: {:#?}", players);
        println!("Score losing * rolls = {:?}", players.iter().filter(|p| p.score < 1000).collect::<Vec<_>>()[0].score * number_of_rolls);
        println!("----------------");
    }

    println!("PART 2");


    let rolls: [(usize, usize); 7] = [
        (3, 1), //3
        (4, 3), //9
        (5, 6), //18
        (6, 7), //21
        (7, 6), //18
        (8, 3), //9
        (9, 1) //3
    ];

    let new_positions: [[usize; NUM_POSITIONS + 1]; NUM_SUMS] = [
        [0,4,5,6,7,8,9,10,1,2,3],
        [0,5,6,7,8,9,10,1,2,3,4],
        [0,6,7,8,9,10,1,2,3,4,5],
        [0,7,8,9,10,1,2,3,4,5,6],
        [0,8,9,10,1,2,3,4,5,6,7],
        [0,9,10,1,2,3,4,5,6,7,8],
        [0,10,1,2,3,4,5,6,7,8,9],
    ];

    let mut start_state_p1: Step = Step {
            position: 4,
            score: 0,
            universes: 1,
            step: 0,
            rolled: 0,
            next: Vec::new()
        };
    let mut start_state_p2: Step = Step {
            position: 8,
            score: 0,
            universes: 1,
            step: 0,
            rolled: 0,
            next: Vec::new()
        };

    let mut step_wins_in_universes_p1: [usize; 10] = [0; 10];
    let mut step_wins_in_universes_p2: [usize; 10] = [0; 10];

    start_state_p1.next = roll_for_step(start_state_p1.clone(), rolls.clone(), new_positions.clone(), 1, &mut step_wins_in_universes_p1);
    start_state_p2.next = roll_for_step(start_state_p2.clone(), rolls.clone(), new_positions.clone(), 1, &mut step_wins_in_universes_p2);
    println!("{:#?}", start_state_p1);

    println!("{:?}", step_wins_in_universes_p1);
    println!("{:?}", step_wins_in_universes_p2);

    let mut accumul_p1 = step_wins_in_universes_p1.clone();
    let mut accumul_p2 = step_wins_in_universes_p2.clone();

    let order: [usize; 7] = [9,8,7,6,5,4,3];
    for index in order.iter() {
        accumul_p1[index - 1] += accumul_p1[*index];
        accumul_p2[index - 1] += accumul_p2[*index];
    }

    println!("p1: {:?}", accumul_p1);
    println!("p2: {:?}", accumul_p2);


    // let p1_pos = 4;
    // let p2_pos = 8;
    // for sum in 3..10 {
    //     println!("Start = {:?}, roll sum = {:?}, new pos = {:?}", p1_pos, sum, new_positions[sum - 3][p1_pos - 1]);
    // }
    // print!("\n");
    // for sum in 3..10 {
    //     println!("Start = {:?}, roll sum = {:?}, new pos = {:?}", p2_pos, sum, new_positions[sum - 3][p2_pos - 1]);
    // }

    // let roll_possibilities: [RollOption; 7] = [
    //     RollOption{sum: 3, new_positions: [0,4,5,6,7,8,9,10,1,2,3], universes_created: 1, next: Vec::new()},
    //     RollOption{sum: 4, new_positions: [0,5,6,7,8,9,10,1,2,3,4], universes_created: 3, next: Vec::new()},
    //     RollOption{sum: 5, new_positions: [0,6,7,8,9,10,1,2,3,4,5], universes_created: 6, next: Vec::new()},
    //     RollOption{sum: 6, new_positions: [0,7,8,9,10,1,2,3,4,5,6], universes_created: 7, next: Vec::new()},
    //     RollOption{sum: 7, new_positions: [0,8,9,10,1,2,3,4,5,6,7], universes_created: 6, next: Vec::new()},
    //     RollOption{sum: 8, new_positions: [0,9,10,1,2,3,4,5,6,7,8], universes_created: 3, next: Vec::new()},
    //     RollOption{sum: 9, new_positions: [0,10,1,2,3,4,5,6,7,8,9], universes_created: 3, next: Vec::new()}
    // ];

/*

    let roll_possibilities: [RollOption; 7] = [
        RollOption{sum: 3, new_positions: [0,4,5,6,7,8,9,10,1,2,3], universes_created: 3},
        RollOption{sum: 4, new_positions: [0,5,6,7,8,9,10,1,2,3,4], universes_created: 9},
        RollOption{sum: 5, new_positions: [0,6,7,8,9,10,1,2,3,4,5], universes_created: 18},
        RollOption{sum: 6, new_positions: [0,7,8,9,10,1,2,3,4,5,6], universes_created: 21},
        RollOption{sum: 7, new_positions: [0,8,9,10,1,2,3,4,5,6,7], universes_created: 18},
        RollOption{sum: 8, new_positions: [0,9,10,1,2,3,4,5,6,7,8], universes_created: 9},
        RollOption{sum: 9, new_positions: [0,10,1,2,3,4,5,6,7,8,9], universes_created: 3}
    ];

    let mut total_universes: usize = 0;
    total_universes += 1;

    // keep track of how many players are in these, max score can be 29, because player can be at 20 points, position 10 and roll a 9
    // on the other axis we need current position, which is 1 to 10

    let mut counter: Vec<PlayerCounter> = Vec::new();

    for pos in 1..11 // pos = y
    {
        for score in 0..30 // score = x
        {
            counter.push(PlayerCounter {
                position: pos,
                score: score,
                amount_p0: [0;10],
                amount_p1: [0;10]
            });
        }
    }

    // set start players
    counter[index_from_position(_p2_players[0].current_position)].amount_p0[0] += 1;
    counter[index_from_position(_p2_players[1].current_position)].amount_p1[0] += 1;


    for u in 1..6 {
        let copy_to_loop_over = counter.clone();
        let roll_for: Vec<&PlayerCounter> = copy_to_loop_over.iter().filter(|p| (p.amount_p0[u - 1] > 0 || p.amount_p1[u - 1] > 0) && p.score < 21).collect::<Vec<_>>();
        for player in roll_for.iter()
        {
            let initial_count_p0 = player.amount_p0[u - 1];
            let initial_count_p1 = player.amount_p1[u - 1];
            if initial_count_p0 > 0
            {
                for roll in roll_possibilities.iter()
                {
                    let new_pos = roll.new_positions[player.position];
                    let new_score = new_pos + player.score;
                    if new_score <= 29
                    {
                        counter[index_from_position(new_pos) + new_score].amount_p0[u] += roll.universes_created.checked_pow(u.try_into().unwrap()).unwrap();
                    }
                }
            }
            if initial_count_p1 > 0
            {
                for roll in roll_possibilities.iter()
                {
                    let new_pos = roll.new_positions[player.position];
                    let new_score = new_pos + player.score;
                    if new_score <= 29
                    {
                        counter[index_from_position(new_pos) + new_score].amount_p1[u] += roll.universes_created.checked_pow(u.try_into().unwrap()).unwrap();
                    }
                }
            }
        }
    }

    // println!("{:#?}", counter.iter().filter(|p| {
    //     p.amount_p0[1] > 0 || p.amount_p1[1] > 0 ||
    //     p.amount_p0[2] > 0 || p.amount_p1[2] > 0 ||
    //     p.amount_p0[3] > 0 || p.amount_p1[3] > 0 ||
    //     p.amount_p0[4] > 0 || p.amount_p1[4] > 0 ||
    //     p.amount_p0[5] > 0 || p.amount_p1[5] > 0
    // }).collect::<Vec<_>>());

    println!("{:#?}", counter.iter().filter(|p| {
        p.score >= 20 &&
        (
            p.amount_p0[1] > 0 || p.amount_p1[1] > 0 ||
            p.amount_p0[2] > 0 || p.amount_p1[2] > 0 ||
            p.amount_p0[3] > 0 || p.amount_p1[3] > 0 ||
            p.amount_p0[4] > 0 || p.amount_p1[4] > 0 ||
            p.amount_p0[5] > 0 || p.amount_p1[5] > 0
        )
    }).collect::<Vec<_>>());


    // println!("{:#?}", counter);
    println!("{:?}", total_universes);

    */

/*
    let mut roll_sums: [usize; 27] = [0; 27];
    roll_sums[0] = 3;
    roll_sums[1] = 4;
    roll_sums[2] = 5;
    roll_sums[3] = 4;
    roll_sums[4] = 5;
    roll_sums[5] = 6;
    roll_sums[6] = 5;
    roll_sums[7] = 6;
    roll_sums[8] = 7;
    roll_sums[9] = 4;
    roll_sums[10] = 5;
    roll_sums[11] = 6;
    roll_sums[12] = 5;
    roll_sums[13] = 6;
    roll_sums[14] = 7;
    roll_sums[15] = 6;
    roll_sums[16] = 7;
    roll_sums[17] = 8;
    roll_sums[18] = 5;
    roll_sums[19] = 6;
    roll_sums[20] = 7;
    roll_sums[21] = 6;
    roll_sums[22] = 7;
    roll_sums[23] = 8;
    roll_sums[24] = 7;
    roll_sums[25] = 8;
    roll_sums[26] = 9;

    for i in 0..27 {
        let mut players_here = _p2_players.clone();
        let dice_sum = roll_sums[i];
        let mut p0_new_position = dice_sum + players_here[0].current_position;
        let mut p1_new_position = dice_sum + players_here[1].current_position;
        if p0_new_position > 10
        {
            p0_new_position %= 10;
        }
        if p1_new_position > 10
        {
            p1_new_position %= 10;
        }
        players_here[0].update_score(p0_new_position);
        players_here[1].update_score(p1_new_position);

        for i2 in 0..27 {
            let mut players_here2 = players_here.clone();
            let dice_sum2 = roll_sums[i2];
            let mut p0_new_pos_2 = dice_sum2 + players_here2[0].current_position;
            let mut p1_new_pos_2 = dice_sum2 + players_here2[1].current_position;
            if p0_new_pos_2 > 10
            {
                p0_new_pos_2 %= 10;
            }
            if p1_new_pos_2 > 10
            {
                p1_new_pos_2 %= 10;
            }
            players_here2[0].update_score(p0_new_pos_2);
            players_here2[1].update_score(p1_new_pos_2);

            for i3 in 0..27 {
                let mut players_here3 = players_here2.clone();
                let dice_sum3 = roll_sums[i3];
                let mut p0_new_pos_3 = dice_sum3 + players_here3[0].current_position;
                let mut p1_new_pos_3 = dice_sum3 + players_here3[1].current_position;
                if p0_new_pos_3 > 10
                {
                    p0_new_pos_3 %= 10;
                }
                if p1_new_pos_3 > 10
                {
                    p1_new_pos_3 %= 10;
                }
                players_here3[0].update_score(p0_new_pos_3);
                players_here3[1].update_score(p1_new_pos_3);
                println!("{:?}", players_here3.iter().map(|p| p.score).collect::<Vec<_>>());
            }
        }
    }

    println!("{:?}", roll_sums);
*/

    Ok(())
}