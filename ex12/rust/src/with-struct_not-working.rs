// use std::collections::HashSet;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

#[derive(Debug)]
struct Node {
	name: String,
	isbig: i32,
	visits: i32,
	neighbours: vec![0; 50]
}

fn main() -> io::Result<()> {
	let f = File::open("../simple")?;
	// let f = File::open("../medium")?;
	// let f = File::open("../large")?;
	// let f = File::open("../input")?;
	let reader = BufReader::new(f);
	let mut lines: Vec<String> = Vec::new();


	let mut data: Vec<Node> = Vec::new();
	let mut count = 0;

	for line in reader.lines() {
		lines.push(line.unwrap());
		let ab = lines[count].split('-').collect::<Vec<&str>>();
		if data.iter().filter(|el| el.name == ab[0]).count() == 0
		{
			data.push({ Node {
				name: ab[0].to_string(),
				visits: 0,
				neighbours: Vec::new()
			}});
		}
		if data.iter().filter(|el| el.name == ab[1]).count() == 0
		{
			data.push({ Node {
				name: ab[1].to_string(),
				visits: 0,
				neighbours: Vec::new()
			}});
		}
		let Node = data.iter().filter(|el| el.name == ab[0]).nth(0).unwrap();
		if left.neighbours.iter().filter(|el| el.name == ab[1]).count() == 0
		{
			left.neighbours.push({ Node {
				name: ab[1].to_string(),
				visits: 0,
				neighbours: Vec::new()
			}})
		}
		println!("left: {:?}", left);
		count = count + 1;
	}

	// for line in lines.iter()
	// {

	// }

	println!("{:?}", data);

	Ok(())
}