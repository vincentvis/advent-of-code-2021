use petgraph::graph::Graph;
use petgraph::dot::Dot;
use petgraph::visit::Bfs;

use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("../simple")?;
    // let f = File::open("../medium")?;
    // let f = File::open("../large")?;
    // let f = File::open("../input")?;
    let reader = BufReader::new(f);
    let mut lines: Vec<String> = Vec::new();

    let mut graph = Graph::<&str, u32>::new();
    let mut nodes: Vec<&str> = Vec::new();
    let mut node_indexies: Vec<_> = Vec::new();
    let mut visits: Vec<i32> = Vec::new();

    for line in reader.lines() {
        lines.push(line.unwrap());
    }

    let mut start_index = 0;
    let mut end_index = 0;

    for line in lines.iter() {
        let a_b = line.split('-').collect::<Vec<&str>>();
        if ! nodes.contains(&a_b[0])
        {
            node_indexies.push(graph.add_node(a_b[0]));
            visits.push(0);
            nodes.push(a_b[0]);
        }
        if ! nodes.contains(&a_b[1])
        {
            node_indexies.push(graph.add_node(a_b[1]));
            visits.push(0);
            nodes.push(a_b[1]);
        }
        let mut start = 0;
        let mut end = 0;
        let mut count = 0;
        for node_str in nodes.iter() {
            if *node_str == a_b[0]
            {
                start = count;
            }
            if *node_str == a_b[1]
            {
                end = count;
            }
            count = count + 1;
        }
        graph.add_edge(node_indexies[start], node_indexies[end], 1);
    }

    {
        let mut count = 0;
        for node_str in nodes.iter() {
            if node_str.starts_with("start")
            {
                start_index = count;
            }
            if node_str.starts_with("end")
            {
                end_index = count;
            }
            count = count + 1;
        }
    }

    println!("start index {:?} end index {:?}", start_index, end_index);
    println!("{}", Dot::new(&graph));
    println!("{:?}", nodes);
    println!("{:?}", node_indexies);
    println!("{:?}", visits);
    for start in graph.node_indices() {
        let mut bfs = Bfs::new(&graph, start);

        print!("[{}] ", start.index());

        while let Some(visited) = bfs.next(&graph) {
            print!(" {}", nodes[visited.index()]);
        }

        println!();
    }

    Ok(())
}