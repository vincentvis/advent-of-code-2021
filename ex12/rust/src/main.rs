// use std::collections::HashSet;
use std::collections::HashMap;
use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

#[derive(Hash, Eq, PartialEq, Debug, Clone)]
pub struct Node {
	name: String,
	isbig: bool,
	visits: i32,
	neighbours: Vec<String>
}

fn main() -> io::Result<()> {
	let f = File::open("../simple")?;
	// let f = File::open("../medium")?;
	// let f = File::open("../large")?;
	// let f = File::open("../input")?;
	let reader = BufReader::new(f);
	let mut lines: Vec<String> = Vec::new();

	// let mut data: Vec<Node> = Vec::new();
	let mut data = HashMap::new();
	let mut count = 0;

	for line in reader.lines() {
		lines.push(line.unwrap());
		let ab = lines[count].split('-').collect::<Vec<&str>>();

		let a_str = ab[0].to_string();
		let b_str = ab[1].to_string();

		let a = data.entry(a_str.clone()).or_insert(Node {
			name: a_str.clone(),
			isbig: ab[0].chars().nth(0).unwrap().is_uppercase(),
			visits: 0,
			neighbours: Vec::new()
		});
		if ! a.neighbours.contains(&b_str)
		{
			a.neighbours.push(b_str.clone());
		}

		let b = data.entry(b_str.clone()).or_insert(Node {
			name: b_str.clone(),
			isbig: ab[1].chars().nth(0).unwrap().is_uppercase(),
			visits: 0,
			neighbours: Vec::new()
		});
		if ! b.neighbours.contains(&a_str)
		{
			b.neighbours.push(a_str.clone());
		}
		count = count + 1;
	}

	// for line in lines.iter()
	// {

	// }
	let mut mapclone = data.clone();
	let start_node = data.get_mut(&"start".to_string()).unwrap();
	let end_node = mapclone.get_mut(&"end".to_string()).unwrap();
	start_node.visits = 1;

	for neigh in start_node.neighbours.iter() {
		let mut path = Vec::new();
		path.push(start_node.name.clone());
		path.push(neigh.clone());
		println!("{:?}", path);
	}


	println!("{:#?}", end_node);

	Ok(())
}