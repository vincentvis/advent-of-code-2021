fs = require('fs');

function isbig(str)
{
    if (str == "start" || str == "end" || (str[0] >= 'A' && str[0] <= 'Z'))
        return (1);
    return (0)
}

function find_path(graph, path_hashes)
{
    let path = [];

    let node_name = "start";
    if (path_hashes.length == 0)
    {
        while (graph.hasOwnProperty(node_name) && graph[node_name].neighs.length)
        {
            path.push(node_name);
            graph[node_name].visists++;
            if (node_name == "end")
                break ;

            let possibilities = graph[node_name].neighs.filter(n => {
                if (graph[n].isbig && n != node_name && n != "start")
                    return true;
                if (!graph[n].isbig && graph[n].visists == 0)
                    return true;
                return false;
            });
            if (possibilities.length)
                node_name = possibilities[0];
            else
                break ;
        }
    }
    return {
        newpath: path,
        newhash: path.join('-')
    };
}

fs.readFile('../simple', 'utf8', function (err,data) {

    let links = data.split('\n').filter(Boolean).map(str => str.split('-'));


    let graph = {};
    links.forEach(link => {
        if (! graph.hasOwnProperty(link[0]))
        {
            graph[link[0]] = {
                self: link[0],
                isbig: isbig(link[0]),
                visists: 0,
                neighs: []
            };
        }
        if (! graph.hasOwnProperty(link[1]))
        {
            graph[link[1]] = {
                self: link[1],
                isbig: isbig(link[1]),
                visists: 0,
                neighs: []
            };
        }
        if (! graph[link[0]].neighs.hasOwnProperty(link[1]))
        {
            graph[link[0]].neighs.push(link[1]);
        }
        if (! graph[link[1]].neighs.hasOwnProperty(link[0]))
        {
            graph[link[1]].neighs.push(link[0]);
        }
    });

    let paths = [];
    let path_hashes = [];
    let new_stuff = find_path(graph, path_hashes);
    paths.push(new_stuff.newpath);
    path_hashes[new_stuff.newhash] = 1;

    console.log(graph);
    console.log(paths);
    // console.log(path_hashes);

});
