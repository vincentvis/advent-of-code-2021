elem = document.querySelector('pre').textContent;
example = `0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
`;

textContainer = elem;
data = textContainer.trim().split('\n').map(line=>line.split(' -> ').map(coord=>coord.split(',').map(str=>Number(str))))
max = Math.max.apply(Math, textContainer.trim().replaceAll(' -> ','\n').replaceAll(',','\n').split('\n').map(n=>Number(n))) + 5;
grid = []; grid.length = max;
for (let i = max; i >= 0; i--)
{
    grid[i] = [];
    grid[i].length = max;
    grid[i].fill(0);
}

data_length = data.length;

// for (let d = data_length - 1; d >= data_length - 3; d--)
for (let d = data_length - 1; d >= 0; d--)
{
    let coords = data[d];
    let start = coords[0];
    let end = coords[1];
    let start_x = start[0];
    let start_y = start[1];
    let end_x = end[0];
    let end_y = end[1];

    grid[start_y][start_x]++;
    while (start_x != end_x || start_y != end_y)
    {
        if (start_x < end_x)
            start_x++;
        else if (start_x > end_x)
            start_x--;

        if (start_y < end_y)
            start_y++;
        else if (start_y > end_y)
            start_y--;

        grid[start_y][start_x]++;
    }
}

should_be_sum = grid.map(row => row.filter(n => n > 1).length).reduce((partial_sum, a) => partial_sum + a, 0);
// grid.forEach((row, index) => console.log(index+": " + row.join('').replaceAll('0','.')))

// 21305 == correct