fs = require('fs');

const hex_to_bin = [
	"0000",
	"0001",
	"0010",
	"0011",
	"0100",
	"0101",
	"0110",
	"0111",
	"1000",
	"1001",
	"1010",
	"1011",
	"1100",
	"1101",
	"1110",
	"1111",
]

const hex_to_bin_index = {"0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "A": 10, "B": 11, "C": 12, "D": 13, "E": 14, "F": 15};

class Packet {
	constructor(version, type)
	{
		this.version = version,
		this.type = type,
		this.bitlabel = false,
		this.value = false
		this.subpackets = []
	}

	set_bitlabel(bitlabel) {
		this.bitlabel = bitlabel
	}

	set_value(value) {
		this.value = value
	}

	add_subpacket(packet) {
		this.subpackets.push(packet);
	}
}

function create_binary_file(line)
{
	chars = line.split('');
	binary = "";
	chars.forEach(c => { binary += hex_to_bin[hex_to_bin_index[c]] });
	return binary;
}

function process_next_packet(binary, offset, packets, max) {

	let version = parseInt(binary.slice(offset, offset + 3), 2);
	offset += 3;
	let type = parseInt(binary.slice(offset, offset + 3), 2);
	offset += 3;

	let packet = new Packet(version, type);

	if (offset >= max)
	{
		packets.add_subpacket(packet);
		return [offset, packets, false];
	}

	if (packet.type != 4)
	{
		packet.set_bitlabel(parseInt(binary.slice(offset, offset + 1), 2));
		offset += 1;
		let len = 11;
		if (packet.bitlabel == 0)
			len = 15;
		packet.set_value(parseInt(binary.slice(offset, offset + len), 2));
		offset += len;

		if (packet.bitlabel == 0)
		{
			binary_substr = binary.slice(offset, offset + packet.value);
			[new_offset, packet, stop] = process_next_packet(binary_substr, 0, packet, binary_substr.length);
			offset += new_offset;
		}
		else
		{
			[offset, packet, stop] = process_next_packet(binary, offset, packet, max);
		}
	}
	else
	{
		let bitstr = "";
		while (1)
		{
			let substr = binary.slice(offset, offset + 5);
			bitstr += substr.slice(1, 5);
			offset += 5;
			if (substr[0] == "0")
				break ;
		}
		packet.set_value(parseInt(bitstr, 2));
	}

	packets.add_subpacket(packet);

	return [offset, packets, false];
}


fs.readFile('../input_binary', 'utf8', function (err,data) {

	line = data.split("\n")[0];
	line = "9C0141080250320F1802104A08";
	binary = create_binary_file(line);
	console.log(binary);
	// binary = line;
	let full_length = binary.length;

	let offset = 0;

	// move through padding
	// while (binary.slice(offset, slice) == "0000")
	// {
	//     offset += 4;
	//     slice += 4;
	// }

	let stop = false
	wrapper_packet = new Packet(0,0);

	while (offset < full_length && stop === false)
	{
		[offset, wrapper_packet, stop] = process_next_packet(binary, offset, wrapper_packet, full_length);
	}

			console.log(JSON.stringify(wrapper_packet))
	// wrapper_packet.subpackets.forEach(packet => {
	// });

});