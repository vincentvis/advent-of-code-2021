use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

// a step:
//  1. increase all by 1
//  2. all > 9 flash
//      2.a all adjecent + diagonal increase by 1
//      goto 2
//  3. any that flashed go to 0

fn show_line(lines: &Vec<Vec<u8>>) {
	for line in lines {
		for c in line {
			if *c > 9 as u8
			{
				print!(".");
			}
			else
			{
				print!("{:?}", c);
			}
		}
		print!("\n");
		// println!("{:?}", line);
	}
}

// fn flash_around(lines: &mut Vec<Vec<u8>>,
// 		y: usize, x: usize,
// 		width: usize, height: usize,
// 		flashes: &mut usize) {
// 	*flashes = *flashes + 1;
// 	lines[y][x] = 1 << 7;
// 	// increase around
// 	if y > 0
// 	{
// 		if lines[y - 1][x] <= 9
// 		{
// 			lines[y - 1][x] = lines[y - 1][x] + 1;
// 			if lines[y - 1][x] >= 10 && lines[y - 1][x] < 20
// 			{
// 				// println!("pre flash {:?}:{:?} ({:?})", y,x, lines[y - 1][x]);
// 				flash_around(lines, y - 1, x, width, height, flashes);
// 				// println!("postflash {:?}:{:?} ({:?})", y,x, lines[y - 1][x]);
// 			}
// 		}
// 		if x > 0 && lines[y - 1][x - 1] <= 9
// 		{
// 			lines[y - 1][x - 1] = lines[y - 1][x - 1] + 1;
// 			if lines[y - 1][x - 1] >= 10 && lines[y - 1][x - 1] < 20
// 			{
// 				flash_around(lines, y - 1, x - 1, width, height, flashes);
// 			}
// 		}
// 		if x < width - 1 && lines[y - 1][x + 1] <= 9
// 		{
// 			lines[y - 1][x + 1] = lines[y - 1][x + 1] + 1;
// 			if lines[y - 1][x + 1] >= 10 && lines[y - 1][x + 1] < 20
// 			{
// 				flash_around(lines, y - 1, x + 1, width, height, flashes);
// 			}
// 		}
// 	}
// 	if y < height - 1
// 	{
// 		if lines[y + 1][x] <= 9
// 		{
// 			lines[y + 1][x] = lines[y + 1][x] + 1;
// 			if lines[y + 1][x] >= 10 && lines[y + 1][x] < 20
// 			{
// 				flash_around(lines, y + 1, x, width, height, flashes);
// 			}
// 		}
// 		if x > 0 && lines[y + 1][x - 1] <= 9
// 		{
// 			lines[y + 1][x - 1] = lines[y + 1][x - 1] + 1;
// 			if lines[y + 1][x - 1] >= 10 && lines[y + 1][x - 1] < 20
// 			{
// 				flash_around(lines, y + 1, x - 1, width, height, flashes);
// 			}
// 		}
// 		if x < width - 1 && lines[y + 1][x + 1] <= 9
// 		{
// 			lines[y + 1][x + 1] = lines[y + 1][x + 1] + 1;
// 			if lines[y + 1][x + 1] >= 10 && lines[y + 1][x + 1] < 20
// 			{
// 				flash_around(lines, y + 1, x + 1, width, height, flashes);
// 			}
// 		}
// 	}
// 	if x > 0 && lines[y][x - 1] <= 9
// 	{
// 		println!("pre flash {:?}:{:?} ({:?})", y,x, lines[y][x - 1]);
// 		lines[y][x - 1] = lines[y][x - 1] + 1;
// 		if lines[y][x - 1] >= 10 && lines[y][x - 1] < 20
// 		{
// 			flash_around(lines, y, x - 1, width, height, flashes);
// 		}
// 		else {
// 			println!("Do not flash around again");
// 		}
// 		println!("postflash {:?}:{:?} ({:?})", y,x, lines[y][x - 1]);
// 	}
// 	if x < width - 1 && lines[y][x + 1] <= 9
// 	{
// 		lines[y][x + 1] = lines[y][x + 1] + 1;
// 		if lines[y][x + 1] >= 10 && lines[y][x + 1] < 20
// 		{
// 			flash_around(lines, y, x + 1, width, height, flashes);
// 		}
// 	}
// }

// fn increment_lines(mut lines: Vec<Vec<u8>>) -> &Vec<Vec<u8>> {
//     lines = lines.iter().map(|line| line.iter().map(|num| num + 1).collect::<Vec<u8>>()).collect::<Vec<Vec<u8>>>();
//     return lines;
// }

fn flash_effect(lines: &mut Vec<Vec<u8>>,
		y: usize, x: usize,
		width: usize, height: usize,
		flashes: &mut usize)
{
	if lines[y][x] > 9
	{
		return
	}
	lines[y][x] = lines[y][x] + 1;
	if lines[y][x] > 9
	{
		lines[y][x] = 1 << 7;
		*flashes = *flashes + 1;
		flash_around(lines, y, x, width, height, flashes);
	}
}

fn flash_around(lines: &mut Vec<Vec<u8>>,
		y: usize, x: usize,
		width: usize, height: usize,
		flashes: &mut usize)
{
	if y > 0
	{
		flash_effect(lines, y - 1, x, width, height, flashes);
		if x > 0
		{
			flash_effect(lines, y - 1, x - 1, width, height, flashes);
		}
		if x < width - 1
		{
			flash_effect(lines, y - 1, x + 1, width, height, flashes);
		}
	}
	if y < height - 1
	{
		flash_effect(lines, y + 1, x, width, height, flashes);
		if x > 0
		{
			flash_effect(lines, y + 1, x - 1, width, height, flashes);
		}
		if x < width - 1
		{
			flash_effect(lines, y + 1, x + 1, width, height, flashes);
		}
	}
	if x > 0
	{
		flash_effect(lines, y, x - 1, width, height, flashes);
	}
	if x < width - 1
	{
		flash_effect(lines, y, x + 1, width, height, flashes);
	}
}

fn main() -> io::Result<()> {
	// let f = File::open("../example")?;
	let f = File::open("../input")?;
	// let f = File::open("../manual")?;
	// let f = File::open("../simple")?;
	let reader = BufReader::new(f);
	let mut lines: Vec<Vec<u8>> = Vec::new();
	let mut flashes: usize = 0;
	let mut flashes_at_100: usize = 0;

	for line in reader.lines() {
		lines.push(line.unwrap().chars().map(|c| c.to_digit(10).unwrap() as u8).collect());
	}

	let width = lines[0].len();
	let height = lines.len();

	let mut sync_iteration = 0;
		let mut total: i32 = 1;

	println!("Before any steps");
	show_line(&lines);
	// println!(" ");
	for iteration in 0..1000 {
		if total == 0
		{
			sync_iteration = iteration;
			show_line(&lines);
			break;
		}
		total = 0;
		// increase all by 1
		for y in 0..height {
			for x in 0..width {
				if lines[y][x] <= 9
				{
					lines[y][x] = lines[y][x] + 1;
					if lines[y][x] > 9
					{
						flashes = flashes + 1;
						flash_around(&mut lines, y, x, width, height, &mut flashes);
					}
				}
			}
		}

		if iteration == 100
		{
			flashes_at_100 = flashes;
		}

		for y in 0..height {
			for x in 0..width {
				if lines[y][x] > 9
				{
					lines[y][x] = 0;
				}
				total = total + lines[y][x] as i32;
			}
		}
		// make sure everhting that flashed goes to zero
		// lines = lines.iter().map(|line| line.iter().map(|num| num & !(1 << 7) as u8).collect::<Vec<u8>>()).collect::<Vec<Vec<u8>>>();
		// println!("After step {:?}", _i + 1);
		// show_line(&lines);
		// println!(" ");

	}

	// println!("{:?}", flashes);
	println!("Flashes at 100: {:?}", flashes_at_100);
	println!("Synced on iteration {:?}", sync_iteration);

	Ok(())
}