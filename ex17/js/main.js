
x_velocity = 20;
max_steps = 300;
possibilities = [];

min_x = 185;
max_x = 221;
min_y = -122;
max_y = -74

while (x_velocity <= max_x + 1)
{
	y_velocity = min_y;
	while (y_velocity < 123)
	{
		// console.log("check y: "+ y_velocity)
		x_pos = 0;
		y_pos = 0;
		y_velo_used = y_velocity;
		x_velo_used = x_velocity;
		steps = 0;
		while (steps < max_steps)
		{
			if (x_pos >= min_x && x_pos <= max_x && y_pos <= max_y && y_pos >= min_y)
			{
				possibilities.push({steps, x_velocity, y_velocity, x_pos, y_pos});
				break ;
			}
			x_velo_used--;
			if (x_velo_used < 0)
				x_velo_used = 0;
			x_pos += x_velo_used;
			y_pos += y_velo_used;
			y_velo_used--;
			steps++;
		}
		y_velocity++;
	}
	x_velocity++;
}
// possibilities.forEach(el => console.log(el));
console.log(possibilities.length)

