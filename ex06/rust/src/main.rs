use std::io;
use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;

fn main() -> io::Result<()> {
    let f = File::open("../input")?;
    let reader = BufReader::new(f);
    let mut data: Vec<i8> = Vec::new();


    for line in reader.lines() {
        data = line.as_ref()
                    .unwrap().split(',')
                    .map(|s| s.parse().unwrap())
                    .collect();
    }

    // smart way thanks to NIELS
    let mut counts: [u128; 10] = [0; 10];

    counts[0] = data.iter().filter(|n| **n == 0).count() as u128;
    counts[1] = data.iter().filter(|n| **n == 1).count() as u128;
    counts[2] = data.iter().filter(|n| **n == 2).count() as u128;
    counts[3] = data.iter().filter(|n| **n == 3).count() as u128;
    counts[4] = data.iter().filter(|n| **n == 4).count() as u128;
    counts[5] = data.iter().filter(|n| **n == 5).count() as u128;
    counts[6] = data.iter().filter(|n| **n == 6).count() as u128;

    for _iteration in 0..255 {
        let dup = counts[0];
        counts[0] = counts[1];
        counts[1] = counts[2];
        counts[2] = counts[3];
        counts[3] = counts[4];
        counts[4] = counts[5];
        counts[5] = counts[6];
        counts[6] = counts[7] + dup;
        counts[7] = counts[8];
        counts[8] = dup;
    }
    println!("sum part2: {:?}", counts.iter().sum::<u128>());

    Ok(())
}

/*
DUMB WAY
fn main() -> io::Result<()> {
    let f = File::open("../input")?;
    let reader = BufReader::new(f);
    let mut data: Vec<i8> = Vec::new();


    for line in reader.lines() {
        data = line.as_ref()
                    .unwrap().split(',')
                    .map(|s| s.parse().unwrap())
                    .collect();
        // num_lines = num_lines + 1;
    }



    for _i in 0..256 {
        println!("it: {:?}", _i);
        let elems_to_add = data.iter().filter(|n| **n == 0).count();
        data = data.iter().map(|n| if n == &0 {6} else {*n - 1}).collect::<Vec<i8>>();
        for _n in 0..elems_to_add {
            data.push(8);
        }
        // println!("{:?} {:?} {:?}", _i, elems_to_add, data.iter().count());
    }

    println!("Num Fish: {:?}", data.iter().count());

    Ok(())
}
 */