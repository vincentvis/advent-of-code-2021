realData = document.querySelector('pre').textContent;
exampleString = "3,4,3,1,2";

useString = realData;

data = useString.split(',').map(s => {
    return {
        'day': Number(s),
        'new': false
    }
});

data = useString.split(',').map(s => Number(s));

iterations = 80;

while (iterations > 0)
{
    let num_fish_now = data.length;
    for (var i = num_fish_now - 1; i >= 0; i--) {
        if (data[i] == 0)
        {
            data[i] = 6;
            data.push(8);
        }
        else
            data[i]--;
    }
    iterations--;
}

console.log(`PartN result: ${data.length}`);